-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2018 at 11:04 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bph`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(11) NOT NULL,
  `activities` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE IF NOT EXISTS `announcements` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `type` int(11) NOT NULL COMMENT '1:seminars, 2: traings',
  `joiner` text NOT NULL,
  `venue` varchar(225) NOT NULL,
  `date` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `name`, `description`, `type`, `joiner`, `venue`, `date`, `created`) VALUES
(2, 'Test Announcement', 'Mga gwapa mo..Lami siyang bayhana.', 1, ',Siegfred Balidbid', 'Naval', '08/08/2018', '2018-02-09 02:13:00'),
(5, 'Announcement', 'Naay tupok run sa brgy lico, \r\nMga bata tupokan apil dagko', 2, ',Siegfred Balidbid', 'Naval', '09/02/2018', '2018-02-09 02:20:21'),
(6, 'Dive', 'Maybe I came on too strong\r\nMaybe I waited too long\r\nMaybe I played my cards wrong\r\nOh just a little bit wrong\r\nBaby I apologise for it', 2, '', 'Baby i just for', '13/02/2018', '2018-02-09 02:53:24');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `created`) VALUES
(1, 'Human Resource Management', '2018-01-24 13:44:39'),
(2, 'Administrative Services', '2018-01-24 01:47:20'),
(3, 'Human Resource Management', '2018-01-24 01:47:03'),
(4, 'Central Information Management', '2018-01-24 01:46:51'),
(5, 'Property and Supply', '2018-01-24 01:46:40'),
(6, 'Budget and Accounting', '2018-01-24 01:46:28'),
(7, 'Special Newborn Care', '2018-01-24 01:46:15');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE IF NOT EXISTS `designations` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `department` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `department`, `status`) VALUES
(1, 'Nurse I', 2, 1),
(2, 'Staff Nurse\r\n', 1, 1),
(3, 'Nursing Attendant ', 2, 1),
(4, 'Administrative Aide I', 5, 1),
(5, 'Nursing Aide I', 6, 1),
(6, 'Chief of Hospital II', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL,
  `firstname` varchar(225) NOT NULL,
  `lastname` varchar(225) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `username` varchar(225) NOT NULL,
  `sex` varchar(20) NOT NULL,
  `civilstatus` varchar(50) NOT NULL,
  `place_of_birth` text NOT NULL,
  `birthdate` varchar(50) NOT NULL,
  `email` varchar(225) NOT NULL,
  `pincode` int(11) NOT NULL,
  `password` varchar(225) NOT NULL DEFAULT '$2y$10$n8lJ/rfCLYHo75cHSUzqg.CDzF7b/JVv/hfDq9bFnCfC12iJXD1BG',
  `employeeid` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `datehired` varchar(50) NOT NULL,
  `designations` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `role` int(2) NOT NULL COMMENT '1: admin, 2: employee',
  `promotions` text NOT NULL,
  `trainings` text NOT NULL,
  `photo` varchar(225) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `basic_salary` float NOT NULL,
  `daily_rate` float NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `firstname`, `lastname`, `middlename`, `username`, `sex`, `civilstatus`, `place_of_birth`, `birthdate`, `email`, `pincode`, `password`, `employeeid`, `department`, `datehired`, `designations`, `phone`, `role`, `promotions`, `trainings`, `photo`, `status`, `basic_salary`, `daily_rate`, `created`) VALUES
(1, 'Burth', 'p0ldo', '', 'test', 'Male', '', '', '14/02/2018', 'test', 22, 'test', 22222, 1, '2018-01-27', 1, 5345345, 0, '', '', '27336230_156771801778232_4116840631182238652_n.jpg', 0, 20000, 100, '2018-02-14 09:18:49'),
(2, 'Karl', 'Kaloy', '', 'sdfsdf', '', '', '', '', 'sfsdf', 33, 'sdfdsf', 33333, 2, '2018-01-27', 2, 564, 0, '', '', '', 0, 30000, 200, '2018-02-14 03:29:36'),
(3, 'Jo Jul', 'Antipala', 'jo', 'dsfsdfdsfsdfsdfsdfdsf', 'Male', 'Single', 'Naval', '14/02/2018', 'employee@gmail.com', 6280, '', 123456, 1, '05/02/2018', 4, 2147483647, 1, 'test', 'test', '27540141_1564608153655194_8013111505566833304_n.jpg', 0, 600000, 5000, '2018-02-14 08:54:23'),
(7, 'Sigfred', 'Veloso', 'S', 'fffffffffff', 'Male', 'Single', 'Bohol Ginhulngan', '', 'admin@gmail.com', 88888, '', 444444, 1, '05/02/2018', 1, 2147483647, 1, 'test', 'test', '', 0, 0, 0, '2018-02-13 03:59:57'),
(10, 'rrrrrrrr', 'rrrrrrrr', 'rrrrrrr', 'rrrrrrr', '', '', '', '', 'rrrrrr@gmail.com', 6911, '', 12121, 1, '08/02/2018', 1, 2147483647, 0, '', '', '', 0, 0, 0, '2018-02-08 05:47:54'),
(12, 'Chin', 'Chin', 'sdfsdf', 'sdfdsf', 'Female', 'Single', 'sdfsdf', '13/02/2018', 'testrt@test.com', 7026, '', 14344, 1, '08/02/2018', 1, 2147483647, 1, 'g', 'd', '', 0, 200000, 10000, '2018-02-13 07:14:35'),
(13, 'Maayo ka', 'Kuno', 'Gwapa', '34324', 'Female', 'Single', 'sdfd', '', 'testrt2@test.com', 4645, '', 0, 1, '08/02/2018', 1, 2147483647, 0, 'test', 'test', '', 0, 0, 0, '2018-02-08 10:43:18'),
(14, 'fdfgfdg', 'fdgfg', 'dfgdg', 'dfgfdg', 'Male', 'Married ', 'fdgf', '', 'dfgfg@test.com', 7564, '', 0, 1, '16/02/2018', 1, 2147483647, 0, 'sdfsdf', 'sdfdsf', '', 0, 0, 0, '2018-02-08 09:53:44'),
(15, 'Roselyn', 'Arevalo', 'sdfsdf', 'sdfdsf', 'Male', 'Single', 'sdfsd', '', 'sdf@test.com', 5209, '$2y$10$n8lJ/rfCLYHo75cHSUzqg.CDzF7b/JVv/hfDq9bFnCfC12iJXD1BG', 99999, 1, '15/02/2018', 1, 2147483647, 1, 'sfsdfsdf', 'sdfsdf', '', 0, 0, 0, '2018-02-09 13:49:08'),
(16, 'Jonalyn', 'Mondejar', 'Pantas', 'sdfsdf', 'Female', 'Single', 'sdfsdf', '', 'test@test123.com', 2474, '$2y$10$n8lJ/rfCLYHo75cHSUzqg.CDzF7b/JVv/hfDq9bFnCfC12iJXD1BG', 98989, 5, '08/01/2018', 1, 2147483647, 2, '3', '53w5', '', 0, 0, 0, '2018-02-11 02:51:01');

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE IF NOT EXISTS `leaves` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `leave_reason` varchar(100) NOT NULL,
  `leave_date` varchar(100) NOT NULL,
  `leave_date_from` varchar(100) NOT NULL,
  `leave_date_to` varchar(100) NOT NULL,
  `leave_approval` int(11) NOT NULL COMMENT '0: pending, 1: approve: 2 rejected',
  `important_comments` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leaves`
--

INSERT INTO `leaves` (`id`, `name`, `userid`, `leave_reason`, `leave_date`, `leave_date_from`, `leave_date_to`, `leave_approval`, `important_comments`, `created`) VALUES
(1, 'TestTest', 123456, 'Sick', '09/02/2018', '09/02/2018', '09/02/2018', 2, 'Important Comments', '2018-02-11 05:44:13'),
(2, 'TestTest', 123456, 'Maternity/Paternity', '09/02/2018', '09/02/2018', '09/02/2018', 2, 'Imporatanti manganak akong uyab', '2018-02-11 05:44:22'),
(3, 'TestTest', 123456, 'Sick', '10/02/2018', '10/02/2018', '10/02/2018', 2, 'Muadto kog manila', '2018-02-11 05:44:32'),
(4, '', 98989, 'Personal Leave', '06/02/2018', '31/01/2018', '05/02/2018', 1, 'test', '2018-02-12 03:02:38');

-- --------------------------------------------------------

--
-- Table structure for table `messaging`
--

CREATE TABLE IF NOT EXISTS `messaging` (
  `id` int(11) NOT NULL,
  `sender` varchar(50) NOT NULL,
  `recipient` text NOT NULL,
  `title` varchar(225) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL,
  `datesent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `datereply` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE IF NOT EXISTS `salary` (
  `id` int(11) NOT NULL,
  `employeeid` int(11) NOT NULL,
  `salary_per_day` int(11) NOT NULL,
  `basic_salary` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`id`, `employeeid`, `salary_per_day`, `basic_salary`) VALUES
(1, 444444, 200, 3000),
(2, 22222, 250, 3000),
(3, 33333, 230, 3500);

-- --------------------------------------------------------

--
-- Table structure for table `timetracker`
--

CREATE TABLE IF NOT EXISTS `timetracker` (
  `id` int(11) NOT NULL,
  `employeeid` varchar(50) NOT NULL,
  `timein` varchar(50) NOT NULL,
  `timeoutpm` varchar(50) NOT NULL,
  `timeinpm` varchar(50) NOT NULL,
  `timeout` varchar(50) NOT NULL,
  `timediffam` varchar(50) NOT NULL,
  `timediffpm` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `undertime` varchar(50) NOT NULL,
  `undertime_total` float NOT NULL,
  `logdate` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timetracker`
--

INSERT INTO `timetracker` (`id`, `employeeid`, `timein`, `timeoutpm`, `timeinpm`, `timeout`, `timediffam`, `timediffpm`, `status`, `undertime`, `undertime_total`, `logdate`, `created`) VALUES
(98, '22222', '08:00:43', '13:47:44', '04:14:13', '11:19:41', '3 hours, 18 minutes, 58 seconds', '', 4, '5 hours,  18 minutes', 62.5, '2018/02/14', '2018-02-14 03:19:41'),
(99, '33333', '01:00:54', '13:47:44', '', '', '56 minutes, 33 seconds', '', 2, '', 0, '2018/02/05', '2018-02-06 06:03:08'),
(101, '444444', '01:01:39', '15:09:50', '', '', '2 hours, 8 minutes, 11 seconds', '', 2, '', 0, '2018/02/05', '2018-02-06 06:03:17'),
(102, '22222', '08:51:37', '13:47:44', '18:07:11', '', '9 hours, 15 minutes, 39 seconds', '', 4, '', 10, '2018/02/05', '2018-02-14 05:13:42'),
(104, '33333', '09:04:19', '14:11:12', '14:11:18', '11:19:45', '2 hours, 15 minutes, 26 seconds', '', 4, '6 hours,  15 minutes', 150, '2018/02/14', '2018-02-14 03:19:45'),
(105, '22222', '14:49:45', '', '', '', '', '', 1, '', 10, '2018/02/07', '2018-02-14 05:13:48'),
(106, '33333', '10:52:53', '', '', '', '', '', 1, '', 10, '2018/02/08', '2018-02-14 05:13:51'),
(107, '123456', '11:16:38', '11:16:45', '', '', '7 seconds', '', 2, '', 0, '2018/02/08', '2018-02-08 03:16:46'),
(108, '33333', '08:00:00', '11:43:10', '11:43:15', '11:43:21', '3 hours, 43 minutes, 21 seconds', '', 4, '', 0, '2018/02/09', '2018-02-09 03:43:48'),
(109, '22222', '10:53:26', '10:53:32', '10:53:37', '10:53:41', '15 seconds', '', 4, '', 0, '2018/02/12', '2018-02-12 02:53:41'),
(110, '33333', '11:33', '11:33', '08:00:00', '11:34:21', '1 minute, 21 seconds', '', 4, '', 0, '2018/02/12', '2018-02-12 03:34:21'),
(111, '22222', '08:00:00', '15:12:18', '15:12:22', '15:12:26', '7 hours, 12 minutes, 26 seconds', '', 4, '', 0, '2018/02/13', '2018-02-13 07:12:26'),
(112, '14344', '15:16:14', '15:16:18', '15:16:21', '15:16:25', '11 seconds', '', 4, '', 0, '2018/02/13', '2018-02-13 07:16:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `employeeid` varchar(150) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '$2y$10$n8lJ/rfCLYHo75cHSUzqg.CDzF7b/JVv/hfDq9bFnCfC12iJXD1BG',
  `designations` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `timetracker` int(11) NOT NULL,
  `role` tinyint(2) NOT NULL COMMENT '1: admin, 2: employee'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employeeid`, `email`, `password`, `designations`, `created`, `modified`, `username`, `timetracker`, `role`) VALUES
(1, '14344', 'admin@gmail.com', '$2y$10$n8lJ/rfCLYHo75cHSUzqg.CDzF7b/JVv/hfDq9bFnCfC12iJXD1BG', 1, '2018-01-18 16:00:00', '2018-01-19 00:00:00', 'Admin', 0, 1),
(3, '123456', 'employee@gmail.com', '$2y$10$n8lJ/rfCLYHo75cHSUzqg.CDzF7b/JVv/hfDq9bFnCfC12iJXD1BG', 1, '2018-02-07 16:00:00', '2018-02-08 00:00:00', 'Employee', 0, 2),
(4, '', 'dfgfg@test.com', '', 1, '2018-02-07 16:00:00', NULL, 'dfgfdg', 0, 0),
(5, '', 'test@test.com', '$2y$10$n8lJ/rfCLYHo75cHSUzqg.CDzF7b/JVv/hfDq9bFnCfC12iJXD1BG', 1, '2018-02-07 16:00:00', NULL, 'sdfdsf', 0, 2),
(6, '98989', 'test@test123.com', '$2y$10$n8lJ/rfCLYHo75cHSUzqg.CDzF7b/JVv/hfDq9bFnCfC12iJXD1BG', 1, NULL, NULL, 'sdfsdf', 0, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timetracker`
--
ALTER TABLE `timetracker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD KEY `designations` (`designations`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `timetracker`
--
ALTER TABLE `timetracker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `designations`
--
ALTER TABLE `designations`
ADD CONSTRAINT `designations_ibfk_1` FOREIGN KEY (`id`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`designations`) REFERENCES `designations` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
