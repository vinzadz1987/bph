<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class UsersController extends AppController
{
	

	public function beforeFilter(Event $event)
	{
		$this->loadModel('Departments');
		$this->loadModel('Designations');
		parent::beforeFilter($event);
		$this->Auth->allow([
			'logout',
			'employees',
			'employeelist',
			'mainmenu',
			'timetracker',
			'departments',
			'adddepartments',
			'editdepartments',
			'deletedepartments',
			'designations',
			'editdesignations',
			'leaves',
			'userslist',
			'deletedesignations',
			'leavelist',
			'announcements',
			'addannouncements',
			'editannouncements',
			'deleteannouncements',
			'leaveactions',
			'activities'
		]);
	}

	public function index(){
		$this->authlogin();
		$this->set('users', $this->Users->find('all'));
		$this->set('announcements', $this->announcements->find('all')->toArray());
	}

	public function mainmenu() {
		if(($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'index']
			);
		}
		$this->set('announcements', $this->announcements->find('all')->toArray());
	}

	public function authlogin() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect('/login/');
		}	
	}
	
	public function login()
	{
		if(($this->request->session()->read('Auth.User'))) {
			return $this->redirect(
				['action' => 'index']
			);
		}	
		$this->viewBuilder()->autoLayout(false);
		if($this->request->is('post')){
			$user = $this->Auth->identify();
			if($user){
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__('Invalid username or password, try again'));
		}
	}

	public function logout(){
		return $this->redirect($this->Auth->logout());
	}

	function timetracker() {
		$timetrackerTable = TableRegistry::get('Timetracker');
		$timetracker = $timetrackerTable->find('all');
		$employees = $this->Users->find('all')->contain(['Designations']);
		$this->set('employees', $employees);
		$this->set('timetracker', $timetracker);
	}

	public function employees() {
		$this->authlogin();
		$employees = $this->Employees->find('all')->contain(['Designations']);
		$makeselect = $this->Departments->find('list',['keyField' => 'id', 'valueField' => 'name']);
		$emp = null;
		$data = $this->request->getData();
		$emp = $this->Employees->newEntity();
		if(isset($data)) {
           $emp = $this->Employees->patchEntity($emp, $data);
           if($this->Employees->save($emp)){
           	$this->Flash->success(__('Employee added successfully.'));
           	return $this->redirect('/users/employees');
           }
		}
		$this->set('designations', $this->Param->selection('Designations'));
		$this->set('department', $this->Param->selection('Departments'));
		$this->set('employees', $employees->toArray());
	}

	public function employeelist() {
		$this->authlogin();
		$employees = $this->Employees->find('all')->contain(['Designations']);
		$this->set('designations', $this->Param->selection('Designations'));
		$this->set('department', $this->Param->selection('Departments'));
		$this->set('employees', $employees->toArray());
	}

	public function leavelist() {
		$this->authlogin();
		$suser = $this->request->session()->read('Auth.User');
		$employees = $this->Employees->find('all');
		$this->set('employees', $employees->toArray());
		$this->set('leaves', $this->Leaves->find('all'));
	}

	public function leaveactions($id = null) {
		$this->authlogin();
		if($this->request->is('ajax')) {
			$user = $this->Leaves->get($id);
			$user->leave_approval = $this->request->data['leave_approval'];
			if ($this->Leaves->save($user)) {
				$this->Flash->success(__('Approved Leave successfully'));
				return $this->redirect('/leavelist/');
			}
		}
	}

	public function departments() {
		$this->authlogin();
		$departments = null;
		$data = $this->request->getData();
		$departments = $this->Departments->newEntity();
		if(isset($data)) {
           $departments = $this->Departments->patchEntity($departments, $data);
           if($this->Departments->save($departments)){
           	$this->Flash->success(__('Department added.'));
           	return $this->redirect('/users/departments');
           }
		}
		$departments = $this->Departments->find('all');
		$this->set('departments', $departments);

	}

	public function editdepartments($id=null){
		$this->authlogin();
		$Departments = $this->Departments->get($id);
		if ($this->request->is(['post', 'put']) ){
			$Departments = $this->Departments->patchEntity($Departments, $this->request->data);
			if ($this->Departments->save($Departments)) {
				$this->Flash->success(__('Department updated.'));
				return $this->redirect('/users/departments');
			}
		}
		$this->set('departments', $Departments);
	}

	public function designations() {
		$designations = null;
		$this->authlogin();
		$designations = $this->Designations->find('all');
		$data = $this->request->getData();
		$desigNewEntity = $this->Designations->newEntity();
		if(isset($data)) {
			$desigNewEntity = $this->Designations->patchEntity($desigNewEntity, $data);
			if($this->Designations->save($desigNewEntity)){
				$this->Flash->success(__('Designation added.'));
				return $this->redirect('/users/designations');
			}
		}
		$this->set('designations', $designations);
	}

	public function editdesignations($id=null){
		$this->authlogin();
		$Designations = $this->Designations->get($id);
		if ($this->request->is(['post', 'put']) ){
			$Designations = $this->Designations->patchEntity($Designations, $this->request->data);
			if ($this->Designations->save($Designations)) {
				$this->Flash->success(__('Designations updated.'));
				return $this->redirect('/users/designations');
			}
		}
		$this->set('designations', $Designations);
	}

	public function announcements() {
		$this->authlogin();
		$this->set('announcements', $this->announcements->find('all'));
	}

	public function addannouncements() {
		$this->authlogin();
		$events = TableRegistry::get('announcements');
		$user = null;
		$article = $events->newEntity();
		$data = $this->request->getData();
		if(isset($data)) {
			$user = $events->patchEntity($article, $data);
			if($events->save($user)){
				$this->Flash->success(__('save successfully.'));
				return $this->redirect('/events/');
			}
		}
		$this->set('announcements',$user);
	}


	public function editannouncements($id = null){
		if(empty($id)){
			throw new NotFoundException;
		}
		$announcements = TableRegistry::get('announcements');
		$dannouncements = $announcements->get($id);
		if ($this->request->is(['post', 'put']) ){
			$dannouncements = $announcements->patchEntity($dannouncements, $this->request->data);
			if ($announcements->save($dannouncements)) {
				$this->Flash->success(__('updated successfully'));
				return $this->redirect('/users/announcements');
			}
		}
		$this->set('announcements',$dannouncements);
	}
	public function deleteannouncements($id = null){
		$announcements = TableRegistry::get('announcements');
		if(empty($id)){
			throw new NotFoundException;
		}
		$ts = $announcements->get($id);
		$result = $announcements->delete($ts);
		if ( $result ){
			$this->Flash->success(__('successfully removed.'));
			return $this->redirect('/users/announcements');
		}
	}

	public function deletedepartments($id = null){
		if(empty($id)){
			throw new NotFoundException;
		}
		$Departments = $this->Departments->get($id);
		$result = $this->Departments->delete($Departments);
		if ($result) {
			$this->Flash->success(__('Department removed.'));
			return $this->redirect('/users/departments');
		}
	}

	public function deletedesignations($id = null){
		if(empty($id)){
			throw new NotFoundException;
		}
		$Departments = $this->Designations->get($id);
		$result = $this->Designations->delete($Departments);
		if ($result) {
			$this->Flash->success(__('Designations removed.'));
			return $this->redirect('/users/designations');
		}
	}

	public function activities()
	{
		$this->set('activities', $this->Activities->find('all')->toArray());
		$this->set('gEmployees', $this->Employees->find('all')->toArray());
	}

	public function userslist() {
		date_default_timezone_set('Asia/Manila');
		$s = $this->request->session()->read('Auth.User'); 
		$timetrackerTable = TableRegistry::get('Timetracker');
		$timetracker = $timetrackerTable->find('all')->where(['employeeid' => $s['employeeid'] ]);
		$employees = $this->Employees->find('all')->contain(['Designations']);

		$this->set('employees', $employees->toArray());
		$this->set('timetracker', $timetracker);
		$this->set('designations', $this->Param->selection('Designations'));
	}

}
