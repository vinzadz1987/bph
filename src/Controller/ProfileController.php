<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\UsersController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class ProfileController extends AppController
{
	

	public function beforeFilter(Event $event)

	{
		parent::beforeFilter($event);
		$this->Auth->allow([
			'employees',
			'employeelist',
			'addemployees',
			'editprofile',
			'uploadphoto'
		]);
	}

	public function index(){
		$employees = null;
		$this->authlogin();
		$sess_user = $this->request->session()->read('Auth.User');
		$employees = $this->Employees->find('all')->contain(['Designations'])->where(['employeeid' => $sess_user['employeeid']]);
		$this->set('employees', $employees->toArray());
	}
	
	public function authlogin() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect('/login/');
		}	
	}
	
	public function login()
	{
		$this->viewBuilder()->autoLayout(false);
		if($this->request->is('post')){
			$user = $this->Auth->identify();
			if($user){
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__('Invalid username or password, try again'));
		}
	}

    public function logout(){
		return $this->redirect($this->Auth->logout());
	}

	public function editprofile( $id = null ) {
		$this->authlogin();
		if(empty($id)) {
			throw new NotFoundException;
			
		}

		$editprofile = $this->Employees->get($id);
		if($this->request->is(['post','put'])) {
			$target_dir = $target_path = WWW_ROOT . 'img/profile_photo/';
			$target_file = $target_dir . basename($_FILES["photo"]["name"]);

			$fNAME   = $_FILES["photo"]["name"];

            move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file);
            $this->request->data['photo']=$fNAME;


			$editprofile = $this->Employees->patchEntity($editprofile, $this->request->data);
			$users = $this->Users->patchEntity($editprofile, $this->request->data);
			if($this->Employees->save($editprofile) && $this->Users->save($editprofile)) {
				$this->Flash->success(__('Updated Profile'));
				return $this->redirect('/editprofile/'.$id);
			}
		}

		$this->set('department', $this->Param->selection('Departments'));
		$this->set('designations', $this->Param->selection('Designations'));
		$this->set('editprofile', $editprofile);

	}

	public function uploadphoto(){
		$employees = null;
		$this->authlogin();
		$sess_user = $this->request->session()->read('Auth.User');
		$employees = $this->Employees->find('all')->where(['employeeid' => $sess_user['employeeid']]);
		$this->set('photo', $employees->toArray());
	}
}

