<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\UsersController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class PayrollController extends AppController
{
	

	public function beforeFilter(Event $event)

	{
		parent::beforeFilter($event);
		$this->Auth->allow([
			'employees',
			'employeelist',
			'addemployees',
			'workingdays',
			'salaryedit',
			'payslip'
		]);
	}

	public function index(){
		// $timetrackerTable = TableRegistry::get('Timetracker');
		// $salaryTable = TableRegistry::get('salary');

		// $employees = null;
		// $this->authlogin();
		// $employees = $this->Employees->find('all')->contain(['Designations']);

		// $countDays = $timetrackerTable->find('all');
		// $countDays->select([
		// 	'employeeid',
		// 	'count' => $countDays->func()->count('*')
		// ])
		// ->group('employeeid');
		// $countDays = $countDays->toArray();

		// $salary = $salaryTable->find('all');


		// $this->set('department', $this->Param->selection('Departments'));
		// $this->set('designations', $this->Param->selection('Designations'));
		// $this->set('employees', $employees);
		// $this->set('countDays', $countDays);
		// $this->set('salary', $salary);
		$this->authlogin();
		$employees = null;
		$conditions =  $this->Employees->find('all')->contain(['Designations'])->order([ 'created' => 'DESC']);
		if($this->request->is('post')) {

			$data = $this->request->data;
			$search = explode('_', $data['search']);
			if(!empty($search[0]) && isset($search[0])) {
				$search_value = $search[0];
				$search_name = $search[1];
				$conditions = $this->Employees->find('all')
					->contain(['Designations'])
					->where([
						''.$search_name.' LIKE' => $search_value 
					])
					->order([ 'created' => 'DESC']);
			} else {
				$conditions = $this->Employees->find('all')->contain(['Designations'])->order([ 'created' => 'DESC']);
			}
		}

		$employees = $conditions;
		$this->set('department', $this->Departments->find('all')->toArray());
		$this->set('employees',$employees);
	}
	
	public function authlogin() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect('/login/');
		}	
	}
	
	public function login()
	{
		$this->viewBuilder()->autoLayout(false);
		if($this->request->is('post')){
			$user = $this->Auth->identify();
			if($user){
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__('Invalid username or password, try again'));
		}
	}

    public function logout(){
		return $this->redirect($this->Auth->logout());
	}

	public function workingdays($id = null) {
		$this->authlogin();
		date_default_timezone_set('Asia/Manila');
		$timetrackerTable = TableRegistry::get('Timetracker');
		$timetracker = $timetrackerTable->find('all')->where( [ 'employeeid' => $id ] );
		$employees = $this->Employees->find('all')->contain(['Designations']);
		
		$countDays = $timetrackerTable->find('all');
		$countDays->select([
			'employeeid',
			'count' => $countDays->func()->count('*')
		])
		->where(['employeeid' => $id ]);

		$this->set('employees', $employees->toArray());
		$this->set('timetracker', $timetracker);
		$this->set('countDays', $countDays->toArray());
	}

	public function salaryedit($id = null) {
	}

	public function payslip($id) {
		$this->authlogin();
		$employees = $this->Employees->find('all')->contain(['Designations'])->where(['employeeid' => $id]);
		$this->set('employees',$employees->toArray());
		$this->set('department', $this->Departments->find('all')->toArray());

		$countDays = $this->Timetracker->find('all');
		$countDays->select([
			'employeeid',
			'count' => $countDays->func()->count('*')
		])
		->where(['employeeid' => $id ]);
		$this->set('countDays', $countDays->toArray());

		$undertimeTotal = $this->Timetracker->find();
		$undertimeTotal->select([
			'undertime_total' =>  $undertimeTotal->func()->sum('undertime_total')
		])
		->where(['employeeid' => $id ]);
		$this->set('total_undertime',$undertimeTotal->toArray());
	}

}

