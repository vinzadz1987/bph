<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\UsersController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class EmployeesController extends AppController
{
	

	public function beforeFilter(Event $event)

	{
		parent::beforeFilter($event);
		$this->Auth->allow([
			'employees',
			'employeelist',
			'addemployees',
			'employeesedit',
			'employeesadd',
			'employeesdtr',
			'employeesdtrall',
			'leave',
			'delete',
			'leaveapplication',
			'addrequestleave',
			'leaveedit',
			'deleteleave',
			'individualdtr'
		]);
	}

	public function index(){
		$this->authlogin();
		$employees = null;
		$employees = $this->Employees->find('all')->contain(['Designations']);
		$this->set('department', $this->Param->selection('Departments'));
		$this->set('designations', $this->Param->selection('Designations'));
		$this->set('employees', $employees);
	}

	public function search() {

	}
	
	public function authlogin() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect('/login/');
		}	
	}
	
	public function login()
	{
		$this->viewBuilder()->autoLayout(false);
		if($this->request->is('post')){
			$user = $this->Auth->identify();
			if($user){
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__('Invalid username or password, try again'));
		}
	}

    public function logout(){
		return $this->redirect($this->Auth->logout());
	}
	
	public function employees(){
		$this->authlogin();
		$employees = null;
		$conditions =  $this->Employees->find('all')->contain(['Designations'])->order([ 'created' => 'DESC']);
		if($this->request->is('post')) {

			$data = $this->request->data;
			$search = explode('_', $data['search']);
			if(!empty($search[0]) && isset($search[0])) {
				$search_value = $search[0];
				$search_name = $search[1];
				$conditions = $this->Employees->find('all')
					->contain(['Designations'])
					->where([
						''.$search_name.' LIKE' => $search_value 
					])
					->order([ 'created' => 'DESC']);
			} else {
				$conditions = $this->Employees->find('all')->contain(['Designations'])->order([ 'created' => 'DESC']);
			}
		}

		$employees = $conditions;
		$this->set('department', $this->Param->selection('Departments'));
		$this->set('designations', $this->Param->selection('Designations'));
		$this->set('autoPIN', $this->generatePIN());
		$this->set('employees',$employees);
	}

	public function employeesadd(){
		$this->authlogin();
		$departments = null;
		$data = $this->request->getData();
		$employeesadd = $this->Employees->newEntity();
		$user = $this->Users->newEntity();
		if(isset($data)) {
           $employees = $this->Employees->patchEntity($employeesadd, $data);
           $user = $this->Users->patchEntity($user,$data);

           $sessUser = $this->request->session()->read('Auth.User');
			$articlesTable = TableRegistry::get('Activities');
			$article = $articlesTable->newEntity();

			$article->userid = $sessUser['employeeid'];
			$article->activities = 'Add employee';
			$article->type = 'All Employee';

			if ($articlesTable->save($article)) {
			    // The $article entity contains the id now
			    // $id = $article->id;
			}


           if($this->Employees->save($employeesadd) && $this->Users->save($user)){
           	$this->Flash->success(__('Employees created successfully.'));
           	return $this->redirect('/employees/');
           }
		}
		$employees = $this->Employees->find('all')->contain(['Designations']);
		$this->set('department', $this->Param->selection('Departments'));
		$this->set('designations', $this->Param->selection('Designations'));
		$this->set('employeesadd', $employeesadd);
		$this->set('autoPIN', $this->generatePIN());
	}

	public function employeesdtr(){

		$this->authlogin();

		$user = $this->request->session()->read('Auth.User');
		$df = $dt = null;
		if(date('d') <= 15) {
			$df = date('Y/m/01');
			$dt = date('Y/m/d');
		} else {
			$df = date('Y/m/15');
			$dt = date('Y/m/d');
		} 

		$timetracker = $this->Timetracker->find('all', array('conditions' =>
			array(
				'employeeid' => $user['employeeid'],
				'logdate >=' => $df,
				'logdate <=' => $dt
			)
		));

		if($this->request->is('post')) {
			$query = $this->Timetracker->find('all', array('conditions' =>
				array(
					'employeeid' => $user['employeeid'],
					'logdate >=' => $this->request->data['datefrom'],
					'logdate <=' => $this->request->data['dateto']
				)
			));
			$timetracker = $query;
		}

		$employees = $this->Employees->find('all')->contain(['Designations'])->where(['employeeid' => $user['employeeid']]);

		$this->set('employees', $employees->toArray());
		$this->set('datefrom', isset($this->request->data['datefrom'])? $this->request->data['datefrom'] : '');
		$this->set('dateto', isset($this->request->data['dateto'])? $this->request->data['dateto'] : '');
		$this->set('timetracker', $timetracker);
	}

	public function employeesdtrall(){
		$this->authlogin();
		$employees = null;
		$conditions =  $this->Employees->find('all')->contain(['Designations'])->order([ 'created' => 'DESC']);
		if($this->request->is('post')) {

			$data = $this->request->data;
			$search = explode('_', $data['search']);
			if(!empty($search[0]) && isset($search[0])) {
				$search_value = $search[0];
				$search_name = $search[1];
				$conditions = $this->Employees->find('all')
					->contain(['Designations'])
					->where([
						''.$search_name.' LIKE' => $search_value 
					])
					->order([ 'created' => 'DESC']);
			} else {
				$conditions = $this->Employees->find('all')->contain(['Designations'])->order([ 'created' => 'DESC']);
			}
		}

		$employees = $conditions;
		$this->set('department', $this->Departments->find('all')->toArray());
		$this->set('employees',$employees);
	}

	public function individualdtr($id = null) {

		$this->authlogin();
		$df = $dt = null;

		if(date('d') <= 15) {
			$df = date('Y/m/01');
			$dt = date('Y/m/d');
		} else {
			$df = date('Y/m/15');
			$dt = date('Y/m/d');
		} 

		$timetracker = $this->Timetracker->find('all', array('conditions' =>
			array(
				'employeeid' => $id,
				'logdate >=' => $df,
				'logdate <=' => $dt
			)
		));

		if($this->request->is('post')) {
			$query = $this->Timetracker->find('all', array('conditions' =>
				array(
					'employeeid' => $id,
					'logdate >=' => $this->request->data['datefrom'],
					'logdate <=' => $this->request->data['dateto']
				)
			));
			$timetracker = $query;
		}

		$employees = $this->Employees->find('all')->contain(['Designations'])->where(['employeeid' => $id]);

		$this->set('employees', $employees->toArray());
		$this->set('datefrom', isset($this->request->data['datefrom'])? $this->request->data['datefrom'] : '');
		$this->set('dateto', isset($this->request->data['dateto'])? $this->request->data['dateto'] : '');
		$this->set('timetracker', $timetracker);

	}

	public function employeesedit( $id = null ) {
		$this->authlogin();
		if(empty($id)) {
			throw new NotFoundException;
			
		}

		$employeesedit = $this->Employees->get($id);

		if($this->request->is(['post','put'])) {
			$employeesedit = $this->Employees->patchEntity($employeesedit, $this->request->data);
			$users = $this->Users->patchEntity($employeesedit, $this->request->data);
			$sessUser = $this->request->session()->read('Auth.User');
			$articlesTable = TableRegistry::get('Activities');
			$article = $articlesTable->newEntity();

			$article->userid = $sessUser['employeeid'];
			$article->activities = 'Edit employee';
			$article->type = 'All Employee';

			if ($articlesTable->save($article)) {
			    // The $article entity contains the id now
			    // $id = $article->id;
			}
			if($this->Employees->save($employeesedit) && $this->Users->save($employeesedit)) {
				$this->Flash->success(__('The Employee has been updated successfully'));
				return $this->redirect('/employees/');
			}
		}

		$this->set('department', $this->Param->selection('Departments'));
		$this->set('designations', $this->Param->selection('Designations'));
		$this->set('employeesedit', $employeesedit);

	}

	public function delete($id) {
		$this->authlogin();
		$employees = $this->Employees->get($id);
		if($this->Employees->delete($employees)) {
			$this->Flash->success(__('The Employee has been deleted successfully'));
			return $this->redirect('/employees/');
		}
	}

	public function generatePIN($digits = 4) {
		
		$pin = "";
		for($i = 0; $i < $digits; $i++) {
			$pin .= mt_rand(0,9);

		}

		return $pin;
	}

	public function employeelist() {
		$this->authlogin();
		$employees = null;
		// $employees = $this->Employees->find('all')->contain(['Designations'])->order([ 'created' => 'DESC']);
		$conditions =  $this->Employees->find('all')->contain(['Designations'])->order([ 'created' => 'DESC']);
		if($this->request->is('post')) {

			$data = $this->request->data;
			$search = explode('_', $data['search']);
			if(!empty($search[0])  && isset($search[0])) {
				$search_value = $search[0];
				$search_name = $search[1];
				$conditions = $this->Employees->find('all')
					->contain(['Designations'])
					->where([
						''.$search_name.' LIKE' => $search_value 
					])
					->order([ 'created' => 'DESC']);
			} else {
				$conditions = $this->Employees->find('all')->contain(['Designations'])->order([ 'created' => 'DESC']);
			}
		}

		$employees = $conditions;
		$this->set('department', $this->Param->selection('Departments'));
		$this->set('designations', $this->Param->selection('Designations'));
		$this->set('autoPIN', $this->generatePIN());
		$this->set('employees', $employees);
	}
	
	public function userslist() {
		$employees = $this->Employees->find('all')->contain(['Designations']);
		$this->set('userslist',$employees);
		$this->set('departments', $this->Param->selection('Departments'));
		$this->set('designations', $this->Param->selection('Designations'));
	}

	public function leave() {
		$this->authlogin();
		$suser = $this->request->session()->read('Auth.User');
		$employees = $this->Employees->find('all')->where(['employeeid' => $suser['employeeid'] ]);
		$this->set('leaves', $this->Leaves->find('all')->where(['userid' => $suser['employeeid'] ]));
		$this->set('employees', $employees->toArray());
	}

	public function leaveapplication() {
		$this->authlogin();
		$sessUser = $this->request->session()->read('Auth.User');

		$employees = $this->Employees->find('all')->where(['employeeid' => $sessUser['employeeid'] ]);
		$this->set('employees', $employees->toArray());
	}

	public function leaveedit( $id = null ) {
		if(empty($id)) {
			throw new NotFoundException;
			
		}

		$leaveedit = $this->Leaves->get($id);

		if($this->request->is(['post','put'])) {
			$leaveedit = $this->Leaves->patchEntity($leaveedit, $this->request->data);
			$users = $this->Leaves->patchEntity($leaveedit, $this->request->data);
			if($this->Leaves->save($leaveedit) && $this->Users->save($leaveedit)) {
				$this->Flash->success(__('The Leave has been updated successfully'));
				return $this->redirect('/leave/');
			}
		}

		$sessUser = $this->request->session()->read('Auth.User');
		$usersTable = TableRegistry::get('Employees');
		$users = $usersTable->get($sessUser['id']);
		$employees = $this->Employees->find('all')->where(['employeeid' => $sessUser['employeeid'] ]);
		$this->set('employees', $employees->toArray());

		$this->set('users', $users);
		$this->set('designations', $this->Designations->find('all')->toArray());

		// $this->set('department', $this->Param->selection('Departments'));
		// $this->set('designations', $this->Param->selection('Designations'));
		$this->set('leaveedit', $leaveedit);

	}

	public function addrequestleave() {
		$this->authlogin();
		$leaves = null;
		$data = $this->request->getData();
		$leaves = $this->Leaves->newEntity();
		if(isset($data)) {
           $leaves = $this->Leaves->patchEntity($leaves, $data);

			$sessUser = $this->request->session()->read('Auth.User');
			$articlesTable = TableRegistry::get('Activities');
			$article = $articlesTable->newEntity();

			$article->userid = $sessUser['employeeid'];
			$article->activities = 'Request a Leave';
			$article->type = 'Leave Request';

			if ($articlesTable->save($article)) {
			    // The $article entity contains the id now
			    // $id = $article->id;
			}

           if($this->Leaves->save($leaves)){
           	$this->Flash->success(__('Requested leave successfully.'));
           	return $this->redirect('/leave/');
           }
		}
	}

	public function deleteleave($id) {
		$this->authlogin();
		$get = $this->Leaves->get($id);
		if($this->Leaves->delete($get)) {
			$this->Flash->success(__('The Leave has been deleted successfully'));
			return $this->redirect('/leave/');
		}
	}


}

