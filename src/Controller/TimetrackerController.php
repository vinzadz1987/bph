<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\UsersController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class TimetrackerController extends AppController
{
	

	public function beforeFilter(Event $event)

	{
		parent::beforeFilter($event);
		$this->Auth->allow([
			'index',
			'addtimelog'
		]);
	}

	public function index(){
		date_default_timezone_set('Asia/Manila');
		$timetrackerTable = TableRegistry::get('Timetracker');
		$timetracker = $timetrackerTable->find('all')->where( [ 'logdate' => date('Y/m/d') ] );
		$employees = $this->Employees->find('all')->contain(['Designations']);
		$this->set('employees', $employees->toArray());
		$this->set('timetracker', $timetracker);
	}

	function addtimelog() {
		date_default_timezone_set('Asia/Manila');
		$timetrackerTable = TableRegistry::get('Timetracker');
		$data = $this->request->getData();

		if(isset($data)) {

			$empTable = TableRegistry::get('Employees');
			$exists = $empTable->exists(['pincode' => isset($data['pincode']) ? $data['pincode'] : '']);

			if($exists){

				$timenow = date('H:i:s',strtotime($data['time']));
				$logdate = date('Y/m/d');
				$settimein =  date('Y/m/d h:i:s',strtotime('08:00:00'));

				$setpmtimeout =  date('Y/m/d h:i:s',strtotime('02:00:00'));
				$setpmtimein =  date('Y/m/d h:i:s',strtotime('03:00:00'));

				$empData = $this->Employees->find('all')->where(['pincode' => $data['pincode']])->toArray();
				$getUserTrack = $timetrackerTable->find('all')->where(['employeeid' => $empData[0]->employeeid, 'logdate' => date('Y/m/d')] )->toArray();

				if( !empty($getUserTrack) ){
					$field = $message = $employeeid = $status = null;
					$prevTimeIn = $getUserTrack[0]->timein;
					// $daily_rate = null;
					// if(isset($daily_rate)) {
					// 	$daily_rate = $empData[0]->daily_rate;
					// }
					if(!empty($getUserTrack[0]->timein) && empty($getUserTrack[0]->timeoutpm)) {
						$field = "timeoutpm";
						$message = "Time out pm successfully";
						$employeeid = $empData[0]->employeeid;
						$status = 2;
					}
					if(!empty($getUserTrack[0]->timein) && !empty($getUserTrack[0]->timeoutpm) && empty($getUserTrack[0]->timeinpm)) {
						$field = "timeinpm";
						$message = "Time in pm successfully";
						$employeeid = $empData[0]->employeeid;
						$status = 3;
					} 
					if(!empty($getUserTrack[0]->timein) && !empty($getUserTrack[0]->timeoutpm) && !empty($getUserTrack[0]->timeinpm) && empty($getUserTrack[0]->timeout)) {
						$field = "timeout";
						$message = "Time out successfully";
						$employeeid = $empData[0]->employeeid;
						$status = 4;
					}

					$this->editlog($field,$message,$timenow,$employeeid,$logdate,$status,$prevTimeIn,$empData[0]->daily_rate);

				} else {

					$query = $timetrackerTable->query();
					$query->insert(['employeeid', 'timein','status','logdate'])
					    ->values([
					        'employeeid' => $empData[0]->employeeid,
					        'timein' => $timenow,
					        'status' => 1,
					        'logdate' => $logdate
					    ])
					    ->execute();
					return $this->redirect('/timetracker/');	

				}

			} 
			else {
				$this->Flash->error(__('Pincode did not match any employees.'));
				return $this->redirect('/timetracker/');
			}
		}
	}

	public function editlog($fieldname,$message,$timenow,$employeeid,$logdate,$status,$prevTimeIn,$daily_rate) {
		$timetrackerTable = TableRegistry::get('Timetracker');
		if(isset($prevTimeIn)) {
			$undertime = $this->undertime($this->dateDiff( $logdate." ".$timenow, $logdate." ".$prevTimeIn), 1,$daily_rate);
			$undertime_total = $this->undertime($this->dateDiff( $logdate." ".$timenow, $logdate." ".$prevTimeIn), 2,$daily_rate);
		}
		
		$query = $timetrackerTable->query();
		$query->update()
			->set([
				$fieldname => $timenow, 
				'timediffam' =>  isset($prevTimeIn)? $this->dateDiff( $logdate." ".$timenow, $logdate." ".$prevTimeIn) : "" ,
				'status' => $status,
				'undertime' => $undertime,
				'undertime_total' => $undertime_total
			])
			->where(['employeeid' => $employeeid, 'logdate' => $logdate])
			->execute();
		$this->Flash->success(__($message));
		return $this->redirect('/timetracker/');

	}

  function undertime($param,$type,$daily_rate) {
  	// hours, minutes, seconds
  	if($type == 1) {
	  	if(preg_match("/(hours|hour)/i", $param)) {
	  		$xtime = explode(',', $param);
	  		$total_hours = explode(' ',$xtime[0]);
	  		$undertime = 0;
	  		if( $total_hours[0] < 8 ) {
	  			$undertime = ( 8 - $total_hours[0] );
	  		}
	  		return $undertime.' '.$total_hours[1].', '.$xtime[1];
	  	} else if(preg_match("/(minutes|minute)/i", $param)){
	  		$xtime = explode(',', $param);
	  		return $xtime[1];
	  	}
	 } else {
	 	if(preg_match("/(hours|hour)/i", $param)) {
	  		$xtime = explode(',', $param);
	  		$total_hours = explode(' ',$xtime[0]);
	  		$undertime = 0;
	  		if( $total_hours[0] < 8 ) {
	  			$undertime = ( 8 - $total_hours[0] );
	  			$calc_daily = floatval($daily_rate / 8);
	  			$calc_un = floatval($calc_daily*$total_hours[0]);
	  		}

	 		return floatval($daily_rate - $calc_un);
	  	}
	 }
  }

  // PHP strtotime compatible strings
  // , 'time_difference' =>  $this->dateDiff($logdate." 12:30:00", $logdate." 12:30:01")
  function dateDiff($time1, $time2, $precision = 6) {
    // If not numeric then convert texts to unix timestamps
    if (!is_int($time1)) {
      $time1 = strtotime($time1);
    }
    if (!is_int($time2)) {
      $time2 = strtotime($time2);
    }

    // If time1 is bigger than time2
    // Then swap time1 and time2
    if ($time1 > $time2) {
      $ttime = $time1;
      $time1 = $time2;
      $time2 = $ttime;
    }

    // Set up intervals and diffs arrays
    $intervals = array('year','month','day','hour','minute','second');
    $diffs = array();

    // Loop thru all intervals
    foreach ($intervals as $interval) {
      // Create temp time from time1 and interval
      $ttime = strtotime('+1 ' . $interval, $time1);
      // Set initial values
      $add = 1;
      $looped = 0;
      // Loop until temp time is smaller than time2
      while ($time2 >= $ttime) {
        // Create new temp time from time1 and interval
        $add++;
        $ttime = strtotime("+" . $add . " " . $interval, $time1);
        $looped++;
      }
 
      $time1 = strtotime("+" . $looped . " " . $interval, $time1);
      $diffs[$interval] = $looped;
    }
    
    $count = 0;
    $times = array();
    // Loop thru all diffs
    foreach ($diffs as $interval => $value) {
      // Break if we have needed precission
      if ($count >= $precision) {
        break;
      }
      // Add value and interval 
      // if value is bigger than 0
      if ($value > 0) {
        // Add s if value is not 1
        if ($value != 1) {
          $interval .= "s";
        }
        // Add value and interval to times array
        $times[] = $value . " " . $interval;
        $count++;
      }
    }

    // Return string with times
    return implode(", ", $times);
  }

}