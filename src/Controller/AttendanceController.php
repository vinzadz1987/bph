<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\UsersController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AttendanceController extends AppController
{
	

	public function beforeFilter(Event $event)

	{
		parent::beforeFilter($event);
		$this->Auth->allow([
			'employees',
			'employeelist',
			'addemployees'
		]);
	}

	public function index(){
		date_default_timezone_set('Asia/Manila');
		$timetrackerTable = TableRegistry::get('Timetracker');
		$timetracker = $timetrackerTable->find('all');
		$employees = $this->Employees->find('all')->contain(['Designations']);

		$this->set('employees', $employees->toArray());
		$this->set('timetracker', $timetracker);
		$this->set('designations', $this->Param->selection('Designations'));
	}
	
	public function authlogin() {
		if(!($this->request->session()->read('Auth.User'))) {
			return $this->redirect('/login/');
		}	
	}
	
	public function login()
	{
		$this->viewBuilder()->autoLayout(false);
		if($this->request->is('post')){
			$user = $this->Auth->identify();
			if($user){
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__('Invalid username or password, try again'));
		}
	}

    public function logout(){
		return $this->redirect($this->Auth->logout());
	}
}

