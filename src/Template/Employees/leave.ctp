 <div class="page-wrapper">
    <div class="content container-fluid">
<div class="">
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<?php $suser = $this->request->session()->read('Auth.User'); ?>
				<div class="x_title">
					<h2>Leave List</h2>
					<a href="<?=BASE_URL?>leaveapplication/"><button class="btn btn-primary pull-right rounded"><i class="fa fa-plus"></i> Apply Leave </button></a>
					<div class="clearfix"></div>
				</div>
				
				<?= $this->Flash->render() ?>
				<div class="x_content">
					<div class="table-responsive">
					<table id="datatable-buttons" class="table table-striped custom-table m-b-0 datatable">
						<thead>
							<tr>
								<th>ID Number</th>
								<th>Name</th>
								<th>Reason</th>
								<th>Leave Date</th>
								<th>Status</th>
								<th style="width: 68px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($leaves as $leave): ?>
							<tr>
								<td>
									<?php echo $leave['userid']; ?>
								</td>
								<td>
									<?php foreach($employees as $emp) { ?>
										<?php if($emp->employeeid == $leave->userid) {?>
											<?php echo $emp->firstname ?> <?php echo $emp->lastname ?>
										<?php } ?>
									<?php } ?>
								</td>
								<td><?=h($leave->leave_reason)?></td>
								<td><?=h($leave->leave_date)?></td>
								<td>
									<?php 
										if ($leave->leave_approval == 0) {
											echo '<span class="label label-default">pending</span>';
										} 
										if ($leave->leave_approval == 1) {
											echo '<span class="label label-success">approved</span>';
										} 
										if ($leave->leave_approval == 3) {
											echo '<span class="label label-danger">rejected</span>';
										}
									?>
								</td>
								<td>
									<?php 
										echo $this->Html->link(
										'<i class="fa fa-pencil" aria-hidden="true"></i>',
										[
											'controller'=>'employees',
											'action'=>'leaveedit',
											h($leave->id)
										],
										['escape' => false, 'class' => 'btn btn-success btn-xs ' ]
										);
									?>
									<?php 
										echo $this->Html->link(
										'<i class="fa fa-trash" aria-hidden="true"></i>',
										[
											'controller'=>'employees',
											'action'=>'deleteleave',
											h($leave->id)
										],
										['confirm' => 'Are you sure you want to delete this leave?', 'escape' => false, 'class' => 'btn btn-danger btn-xs ' ]
										);
									?>
								</td>
							</tr>
							<?php endforeach; ?> 
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
