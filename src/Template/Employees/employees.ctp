  <div class="page-wrapper">
    <div class="content container-fluid">
		<div class="row">
			<div class="col-xs-4">
				<h4 class="page-title">Employee</h4>
			</div>
			<div class="col-xs-8 text-right m-b-20">
				<?php
					echo $this->Html->link(
						'<i class="fa fa-plus m-r-5"></i> Add Employee',
						[
							'controller' => 'Employees',
							'action' => 'employeesadd'
						],
						[ 'escape' => false, 'class' => 'btn btn-primary rounded pull-right baction' ]
					)
				?>
				<div class="view-icons">
					<a href="<?= BASE_URL.'employees' ?>" class="grid-view btn btn-link active"><i class="fa fa-th"></i></a>
					<a href="<?= BASE_URL.'employeelist' ?>" class="list-view btn btn-link"><i class="fa fa-bars"></i></a>
				</div>
			</div>
		</div>
		<?= $this->Flash->render() ?>

		<div class="row filter-row">

			<?= $this->Form->create('employees',['class' =>'search']) ?>
			<div class="col-sm-3 col-xs-6">  
				<div class="form-group form-focus">
					<label class="control-label">Employee ID</label>
					<?php echo $this->Form->input('employeeid',['class' => 'form-control floating','label' => false, 'data-id' => 'employeeid']);  ?>
				</div>
			</div>
			<div class="col-sm-3 col-xs-6">  
				<div class="form-group form-focus">
					<label class="control-label">Employee Name</label>
					<?php echo $this->Form->input('firstname',['class' => 'form-control floating','label' => false, 'data-id' => 'firstname']);  ?>
				</div>
			</div>
			<div class="col-sm-3 col-xs-6"> 
				<div class="form-group form-focus select-focus">
					<label class="control-label">Designation</label>
					<?php echo  $this->Form->select('designations', $designations, [ 'class' => 'form-control department select floating' ]); ?>
				</div>
			</div>
			<?php echo $this->Form->input('search',['type' => 'hidden']);  ?>
			<div class="col-sm-3 col-xs-6">  
				<?= $this->Form->button(__('Submit'),['class' => 'btn btn-success btn-block', 'data-id' => 'designations']) ?>
				<?= $this->Form->end() ?>
			</div>  

		</div>



		<div class="row staff-grid-row">
			<?php  foreach($employees as $emp) { ?>
				<div class="col-md-4 col-sm-4 col-xs-6 col-lg-3">
					<div class="profile-widget">
						<div class="profile-img">
							<?php if(!empty($emp->photo)) { ?>
								<a href="<?=BASE_URL?>profile/<?=$emp->id?>" class="avatar">
									<img class="avatar" src="<?php echo $this->request->webroot; ?>img/profile_photo/<?=$emp->photo?>" alt="">
								</a>
							<?php } else { ?>
								<a href="<?=BASE_URL?>profile/<?=$emp->id?>" class="avatar">
									<?= substr($emp->firstname, 0, 1).''.substr($emp->lastname, 0, 1) ?>
								</a>
							<?php } ?>
						</div>
						<div class="dropdown profile-action">
							<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
							<ul class="dropdown-menu pull-right">
								<li>
                    				<?php
                    					echo $this->Html->link(
                    						'<i class="fa fa-pencil m-r-5"></i> Edit',
                    						[
                    							'controller' => 'Employees',
                    							'action' => 'employeesedit',
                    							$emp->id
                    						],
                    						[ 'escape' => false ]
                    					)
                    				?>
								</li>
								<li>
									<?php echo $this->Html->link(
										'<i class="fa fa-trash-o m-r-5"></i> Delete',
										['controller' => 'Employees', 'action' => 'delete', $emp->id],
										['confirm' => 'Are you sure you wish to deactivated the employee?', 'escape' => false]
									)?>
								</li>
							</ul>
						</div>
						<h4 class="user-name m-t-10 m-b-0 text-ellipsis"><a href="profile.html"><?= ucfirst($emp->firstname) ?> <?= ucfirst($emp->lastname) ?></a></h4>
						<div class="small text-muted"><?= strtoupper($emp->designation->name) ?></div>
					</div>
				</div>
			<?php } ?>
		</div>
    </div>
</div>
<script type="text/javascript">
	$(function () {
		$(".baction").click( function() {
			if($(this).data('action') == 'edit') {
				$("#addEmployeefrm").attr('action','<?=BASE_URL?>employees/'+$(this).data('id'));
			}
		});
		$("#employeeid,#firstname").keyup( function() {
			$("#search").val($(this).val()+'_'+$(this).data('id'));
		});
		$('select[name="designations"]').change( function() {
			$("#search").val($(this).val()+'_'+$(this).attr('name'));
			$("form.search").submit();
		});
	});
</script>

