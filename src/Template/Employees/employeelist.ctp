<div class="page-wrapper">
	<div class="content container-fluid">
		<div class="row">
			<div class="col-xs-4">
				<h4 class="page-title">Employee</h4>
			</div>
			<div class="col-xs-8 text-right m-b-30">
				<a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Add Employee</a>
				<div class="view-icons">
					<a href="<?= BASE_URL.'employees' ?>" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
					<a href="<?= BASE_URL.'employeelist' ?>" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
				</div>
			</div>
		</div>
		<div class="row filter-row">
		 	<div class="row filter-row">

				<?= $this->Form->create('employees',['class' =>'search']) ?>
				<div class="col-sm-3 col-xs-6">  
					<div class="form-group form-focus">
						<label class="control-label">Employee ID</label>
						<?php echo $this->Form->input('employeeid',['class' => 'form-control floating','label' => false, 'data-id' => 'employeeid']);  ?>
					</div>
				</div>
				<div class="col-sm-3 col-xs-6">  
					<div class="form-group form-focus">
						<label class="control-label">Employee Name</label>
						<?php echo $this->Form->input('firstname',['class' => 'form-control floating','label' => false, 'data-id' => 'firstname']);  ?>
					</div>
				</div>
				<div class="col-sm-3 col-xs-6"> 
					<div class="form-group form-focus select-focus">
						<label class="control-label">Designation</label>
						<?php echo  $this->Form->select('designations', $designations, [ 'class' => 'form-control department select floating' ]); ?>
					</div>
				</div>
				<?php echo $this->Form->input('search',['type' => 'hidden']);  ?>
				<div class="col-sm-3 col-xs-6">  
					<?= $this->Form->button(__('Submit'),['class' => 'btn btn-success btn-block', 'data-id' => 'designations']) ?>
					<?= $this->Form->end() ?>
				</div>  

			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-striped custom-table employeeTable">
						<thead>
							<tr>
								<th style="width:30%;">Name</th>
								<th>Employee ID</th>
								<th>PIN</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Join Date</th>
								<th class="text-right">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($employees as $emp) { ?>
								<tr>
									<td>
										<?php if(!empty($emp->photo)) { ?>
											<a href="<?=BASE_URL?>profile/<?=$emp->id?>" class="avatar">
												<img class="avatar" src="<?php echo $this->request->webroot; ?>img/profile_photo/<?=$emp->photo?>" alt="">
											</a>
										<?php } else { ?>
											<a href="<?=BASE_URL?>profile/<?=$emp->id?>" class="avatar">
												<?= substr($emp->firstname, 0, 1).''.substr($emp->lastname, 0, 1) ?>
											</a>
										<?php } ?>
										<!-- <a href="profile.html" class="avatar"><?= substr($emp->firstname, 0, 1).''.substr($emp->lastname, 0, 1) ?></a> -->
										<h2><a href="profile.html"><?= ucfirst($emp->firstname) ?> <?= ucfirst($emp->lastname) ?><span><?= ucfirst($emp->designation->name) ?></span></a></h2>
									</td>
									<td><?=$emp->pincode?></td>
									<td><?=$emp->employeeid?></td>
									<td><?=$emp->email?></td>
									<td><?=$emp->phone?></td>
									<td><?=$emp->created?></td>
									<td class="text-right">
										<div class="dropdown">
											<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
											<ul class="dropdown-menu pull-right">
												<li>
													<?php
					                					echo $this->Html->link(
					                						'<i class="fa fa-pencil m-r-5"></i> Edit',
					                						[
					                							'controller' => 'Employees',
					                							'action' => 'employeesedit',
					                							$emp->id
					                						],
					                						[ 'escape' => false ]
					                					)
					                				?>
												</li>
												<li>
													<?php echo $this->Html->link(
														'<i class="fa fa-trash-o m-r-5"></i> Delete',
														['controller' => 'Employees', 'action' => 'delete', $emp->id],
														['confirm' => 'Are you sure you wish to deactivated the employee?', 'escape' => false]
													)?>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$("#employeeid,#firstname").keyup( function() {
			$("#search").val($(this).val()+'_'+$(this).data('id'));
		});
		$('select[name="designations"]').change( function() {
			$("#search").val($(this).val()+'_'+$(this).attr('name'));
			$("form.search").submit();
		});
	});
</script>
<script type="text/javascript">
	$('.employeeTable').dataTable( {
		'language': {
			'searchPlaceholder': 'Name, Designation, Time in ....'
		}
	});
</script>
