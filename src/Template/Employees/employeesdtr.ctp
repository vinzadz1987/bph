<!-- Include Date Range Picker -->
<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

  <div class="page-wrapper" style="background-color: #EFF3F6">
    <div class="content container-fluid">

			<center><div class="title"><h3>Daily Time Record</h3></div></center>

				<div class="row">

					<div class="col-sm-12 col-xs-6">  
						<h4>Name: <?= ucfirst($employees[0]->firstname); ?> <?= ucfirst($employees[0]->lastname); ?></h4>
					</div>
					<?= $this->Form->create('individualdtr',['class' =>'search']) ?>
					<div class="col-sm-3 col-xs-6">  
						<div class="form-group form-focus">
							<label class="control-label">Date From - Date To</label>
							<?php 
								date_default_timezone_set('Asia/Manila'); 
								if(date('d') <= 15) {
									if(isset($datefrom) && !empty($datefrom)) {
										$dx = explode('/', $datefrom); 
										$df = $dx[1].'/'.$dx[2].'/'.$dx[0];	
										$dxt = explode('/', $dateto); 
										$dt = $dxt[1].'/'.$dxt[2].'/'.$dxt[0];	
										$range = $df.' - '.$dt;
									} else {
										$range = date('m/01/Y').' - '.date('m/d/Y'); 
									}
								} else {
									if(isset($datefrom) && !empty($datefrom)) {
										$dx = explode('/', $datefrom); 
										$df = $dx[1].'/'.$dx[2].'/'.$dx[0];	
										$dxt = explode('/', $dateto); 
										$dt = $dxt[1].'/'.$dxt[2].'/'.$dxt[0];	
										$range = $df.' - '.$dt;
									} else {
										$range = date('m/15/Y').' - '.date('m/d/Y'); 
									}
								} 
								?>
							<?php echo $this->Form->input('daterange',['class' => 'form-control floating','label' => false, 'value' =>$range  ]);  ?>
						</div>
					</div>
					<?php echo $this->Form->input('datefrom',['type' => 'hidden']);  ?>
					<?php echo $this->Form->input('dateto',['type' => 'hidden']);  ?>
					<div class="col-sm-3 col-xs-6">  
						<?= $this->Form->button(__('Submit'),['class' => 'hidden']) ?>
						<?= $this->Form->end() ?>
					</div>  
					<br><br><br><br><br><br>


					<?= $this->Form->button(__('Print'),['class' => 'btn btn-info pull-right']) ?>
					<table class="table table-striped custom-table datatable">
						<thead>
							<tr>
								<th>Day</th>
								<th>Time In</th>
								<th>Time Out PM</th>
								<th>Time In PM</th>
								<th>Time Out</th>
								<th>Undertime</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($timetracker as $emptime) { ?>
								<tr>
									<td><?php $x = explode('/', $emptime->logdate); echo $x[2]; ?></td>
									<td><?= $emptime->timein ?></td>
									<td><?= $emptime->timeoutpm ?></td>
									<td><?= $emptime->timeinpm ?></td>
									<td><?= $emptime->timeout ?></td>
									<td>
										<!-- <?php echo $emptime->timediffam?> -->
										<?php $u = ""; $u = explode(' ', $emptime->timediffam);
											$min = $tot_un = ":00";
											if($u[0] < 8 ) {
												if(isset($u[2]) && !empty($u[2])) {
													$min = ':'.$u[2];
												}
												$tot_un = intval(8 - $u[0]);
												if($tot_un > 1 ) {
													$tot_un = $tot_un;
												} else {
													$tot_un = (8 - $u[0]).' hr ';
												}
												echo $tot_un.''.$min;

											} else if($u[0] > 8 ) {
												echo 0;
											}
										 ?>
									 	
									 </td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
</div>
</div>
<script type="text/javascript">
	$(function () {
		$('input[name="daterange"]').daterangepicker(
		{
		    locale: {
		      format: 'MM/DD/YYYY'
		    }
		}, 
		function(start, end, label) {
			$('#datefrom').val(start.format('YYYY/MM/DD'));
			$('#dateto').val(end.format('YYYY/MM/DD'));
			$("form.search").submit();
		    
		});
	});
</script>