<style type="text/css">
	.error-message {
		font-weight: bold;
		color: red;
	}
</style>
<div id="add_employee" class="modal custom-modal fade" role="dialog">
	<div class="modal-dialog">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<div class="modal-content modal-lg">
			<div class="modal-body">
					<div class="row">
						<?= $this->Form->create($employeesedit) ?>
						<h3 class="page-title">Edit Employee</h3>
						<h3 class="page-sub-title">Employee Information</h3>
						<!-- <hr> -->
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('firstname', [  'class' => 'form-control', 'label' => 'First Name*' ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('lastname', [  'class' => 'form-control', 'label' => 'Last Name*'  ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('middlename', [  'class' => 'form-control', 'label' => 'Middle Name*'  ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('birthdate', [  'class' => 'form-control datetimepicker', 'label' => 'Birthday*'  ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('place_of_birth', [  'class' => 'form-control', 'type' => 'text' ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php 
									$status = [
												'' => 'Select Civil Status',
												'Single' => 'Single',
												'Married ' => 'Married ',
												'Divorced ' => 'Divorced',
												'Separated ' => 'Separated ',
												'Widowed' => 'Widowed'
											];
									echo $this->Form->select('civilstatus', $status, ['default' => '', 'class' => 'form-control select']); 
								?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php 
									$sex = [
												'' => 'Select Gender',
												'Male' => 'Male',
												'Female' => 'Female'
											];
									echo $this->Form->select('sex', $sex, ['default' => '', 'class' => 'form-control select']); 
								?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('age', [  'class' => 'form-control', 'label' => 'Age*'  ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('username', [  'class' => 'form-control', 'label' => 'Username*' ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('email', [  'class' => 'form-control','type' => 'email', 'label' => 'Email*' ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('pincode', [  'class' => 'form-control','type' => 'number', 'label' => 'Pincode*']); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('employeeid', [  'class' => 'form-control','type' => 'number', 'label' => 'Employee ID*']); ?>
							</div>
						</div>
						<div class="col-sm-6">  
							<div class="form-group">
								<?= $this->Form->control('datehired', [  'class' => 'form-control datetimepicker', 'type' => 'text', 'label' => 'Date Hired*' ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('phone', [  'class' => 'form-control', 'label' => 'Phone*' ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">Department*</label>
								<?php echo  $this->Form->select('department', $department, [ 'class' => 'form-control department' ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">Designation*</label>
								<?php echo  $this->Form->select('designations', $designations, [ 'class' => 'form-control department' ]); ?>
							</div>
						</div>
						<h3 class="page-sub-title">Employment Information</h3>
						<div class="col-sm-6">  
							<div class="form-group">
								<?= $this->Form->control('datehired', [  'class' => 'form-control datetimepicker', 'type' => 'text', 'label' => 'Date Hired*' ]); ?>
							</div>
							<div class="form-group">
								<?php 
									$role = [
												'' => 'Select Role',
												1 => 'Admin',
												2 => 'Employee'
											];
									echo $this->Form->select('role', $role, ['default' => '', 'class' => 'form-control select']); 
								?>
							</div>
						</div>
						<div class="col-sm-6">  
							<div class="form-group">
								<label>Promotions</label>
								<?= $this->Form->textarea('promotions', [  'class' => 'form-control']); ?>
							</div>
							<div class="form-group">
								<label>Trainings / Seminars and Awards</label>
								<?= $this->Form->textarea('trainings', [  'class' => 'form-control']); ?>
							</div>
						</div>
						<h3 class="page-sub-title">Salary Information</h3>
						<div class="col-sm-6">  
							<div class="form-group">
								<?= $this->Form->control('basic_salary', [  'class' => 'form-control', 'label' => 'Basic Salary*' ]); ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $this->Form->control('daily_rate', [  'class' => 'form-control', 'label' => 'Daily Rate*' ]); ?>
							</div>
						</div
						<div class="m-t-20 text-center">
							<?= $this->Form->button(__('Create Employee'),['class'=>'btn btn-primary'] ); ?>
						</div>
					<?= $this->Form->end() ?>
				</div>
		</div>
	</div>
</div>
<a href="#" class="baction hidden" data-toggle="modal" data-target="#add_employee" data-action="add"></a>
<script type="text/javascript">
	$(function () {
		$('.close').click( function() {
			location.href = '<?=BASE_URL?>employees';
		});
		$(".baction").click();
		// $("#phone").mask("999999999999")
		$('.department').select2({
			placeholder:'Enter Designation',
			allowClear: true,
			width: 430
		});
		if($('.errorAlert').hasClass('alert-danger') == true) {
			$("#add_employee").modal({
				show: 'true'
			});
		}

	});
</script>