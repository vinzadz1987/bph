<style type="text/css">
	.error-message {
		font-weight: bold;
		color: red;
	}
</style>
<div id="leaveapplication" class="modal custom-modal fade" role="dialog">
	<div class="modal-dialog">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<div class="modal-content modal-lg">
			<div class="modal-body">
					<div class="row">
						<div class="card-body">
					<?= $this->Form->create($leaveedit) ?>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<p>Employee Name: <h2><?= ucfirst($employees[0]->firstname) ?> <?= ucfirst($employees[0]->lastname) ?> </h2></p>
								<?= $this->Form->control('userid', ['type' => 'hidden', 'value' => $employees[0]->employeeid]); ?>
								<?= $this->Form->control('name', ['type' => 'hidden', 'value' => "".ucfirst($users['firstname'])."".ucfirst($users['lastname'])]); ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<label>Reason of Leave</label>
								<?php 
									$reason = [
												'' => 'Select Leave Reason',
												'Sick' => 'Sick',
												'Bereavement' => 'Bereavement',
												'Unpaid Leave' => 'Unpaid Leave',
												'Personal Leave' => 'Personal Leave',
												'Maternity/Paternity' => 'Maternity/Paternity',
												'Other' => 'Other'
											];
									echo $this->Form->select('leave_reason', $reason, ['default' => '', 'class' => 'form-control select']); 
								?>
							</div>
							<div class="col-md-6">
								<?= $this->Form->control('leave_date', ['label' => 'Date Requested', 'class' => 'form-control datetimepicker col-md-4 col-xs-12 input-sm', 'type' => 'text', 'required' => true]); ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<p>Date of Leave: </p>
								<?= $this->Form->control('leave_date_from', ['label' => 'From', 'class' => 'form-control col-md-4 datetimepicker col-xs-12 input-sm', 'type' => 'text', 'required' => true]); ?> 
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6" style="margin-top: 29px;">
								<?= $this->Form->control('leave_date_to', ['label' => 'To', 'class' => 'form-control datetimepicker col-md-4 col-xs-12 input-sm', 'type' => 'text', 'required' => true]); ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-12">
								<?= $this->Form->control('important_comments', ['label' => 'Important Comments', 'class' => 'form-control col-md-4 col-xs-12 input-sm', 'type' => 'textarea', 'required' => true]); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="m-t-20 text-center">
						<?= $this->Form->button(__('Request'),['class'=>'btn btn-primary','style' => 'margin-top: 10px' ] ); ?>
					</div>
				<div class="form-group">
				<?= $this->Form->end() ?>
				</div>
		</div>
	</div>
</div>
<a href="#" class="baction hidden" data-toggle="modal" data-target="#leaveapplication" data-action="add"></a>
<script type="text/javascript">
	$(function () {
		$('.close').click( function() {
			location.href = '<?=BASE_URL?>leave';
		});
		$(".baction").click();
		$("#phone").mask("999999999999")
		$('.department').select2({
			placeholder:'Enter Designation',
			allowClear: true,
			width: 430
		});
		if($('.errorAlert').hasClass('alert-danger') == true) {
			$("#add_employee").modal({
				show: 'true'
			});
		}

	});
</script>









<!-- 
<div style="height: 50px"></div>
<div class="modal-header">
	<h4 class="modal-title" id="myModalLabel">Request Leave</h4>
</div>
<div class="modal-body">
	<div class="col-md-12 col-sm-3 col-xs-12">
		<section class="x_panel ">
			<div class="x_title">
				<h2>LEAVE APPLICATION FORM </h2>
				<a href="teacherleavelist"><button class="btn btn-primary pull-right"> <i class="fa fa-list"></i> Leave List </button></a>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body">
				<div class="card-body">
					<?= $this->Form->create($users, [ 'class' => 'form', 'url' => '/users/teacherAddRequestLeave' ]) ?>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<p>Employee Name: <h2><?= ucfirst($users['firstname']) ?> <?= ucfirst($users['lastname']) ?> </h2></p>
								<?= $this->Form->control('userid', ['type' => 'hidden']); ?>
								<?= $this->Form->control('name', ['type' => 'hidden', 'value' => "".ucfirst($users['firstname'])."".ucfirst($users['lastname'])]); ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<p>Department : <h2> <?= ucfirst($users['department']) ?></h2></p>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<p>Designation: <h2> <?= ucfirst($users['designation']) ?> </h2></p>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<p>Employment Number : <h2><?= ucfirst($users['userid']) ?></h2></p>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<?= $this->Form->input('leave_reason', [
										'type' => 'select',
										'class' => 'form-control input-sm',
										'label'=> 'Reason for Requested Leave',
										'required' => true,
										'options' => [
											'' => 'Select Leave Reason',
											'Sick' => 'Sick',
											'Bereavement' => 'Bereavement',
											'Unpaid Leave' => 'Unpaid Leave',
											'Personal Leave' => 'Personal Leave',
											'Maternity/Paternity' => 'Maternity/Paternity',
											'Other' => 'Other'
										]
									]);
								?>
							</div>
							<div class="col-md-6">
								<?= $this->Form->control('leave_date', ['label' => 'Date', 'class' => 'form-control col-md-4 col-xs-12 input-sm', 'type' => 'text', 'required' => true]); ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<p>Date Requested: </p>
								<?= $this->Form->control('leave_date_from', ['label' => 'From', 'class' => 'form-control col-md-4 col-xs-12 input-sm', 'type' => 'text', 'required' => true]); ?> 
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6" style="margin-top: 29px;">
								<?= $this->Form->control('leave_date_to', ['label' => 'To', 'class' => 'form-control col-md-4 col-xs-12 input-sm', 'type' => 'text', 'required' => true]); ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-12">
								<?= $this->Form->control('important_comments', ['label' => 'Important Comments', 'class' => 'form-control col-md-4 col-xs-12 input-sm', 'type' => 'textarea', 'required' => true]); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="modal-footer">
			<?php 
			echo $this->Html->link(
				'<i class="fa fa-close" aria-hidden="true"></i> Close',
				[
				'controller'=>'users',
				'action'=>'teacherleavelist'
				],
				['escape' => false, 'class' => 'btn btn-danger' ]
			);
			?>
			<?= $this->Form->button(__('Request'),['class'=>'btn btn-primary','id' => 'saveUser']); ?>
			<?= $this->Form->end() ?>
		</div>
	</div>
	<script type="text/javascript">
		$("#leave-date-from,#leave-date-to,#leave-date").datepicker({
			autoclose: true,
			todayHighlight: false
		});
	</script>
</div> -->
