<?php
  $session = $this->request->session();
  $user_data = $session->read('Auth.User');
  if(!empty($user_data)) {
?>
<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
		<div id="sidebar-menu" class="sidebar-menu">
			<ul class="main-sidebar">
				<?php 
					$directoryURI = $_SERVER['REQUEST_URI'];
					$path = parse_url($directoryURI, PHP_URL_PATH);
					$components = explode('/', $path);
					$last_part = $components[2];
					?>
				<?php if($user_data['role'] == 1 ) { ?>
					<li class="<?php echo ($last_part == "index")? 'active' : '' ?>"> 
						<a class="<?php echo ($last_part == "index")? 'active' : '' ?>" href="<?php BASE_URL; ?>index">Dashboard</a>
					</li>
					<li class="submenu">
						<a href="#" class="noti-dot"><span> Employees</span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled" style="display: none;">
							<li><a class="employees <?php echo ($last_part == "employees")? 'active' : '' ?>" href="<?= BASE_URL ?>employees">All Employees</a></li>
							<li><a class="leaves <?php echo ($last_part == "leavelist")? 'active' : '' ?>" href="<?= BASE_URL ?>leavelist"><span>Leave Requests</span> <span class="badge bg-primary pull-right">1</span></a></li>
							<li><a class="attendance <?php echo ($last_part == "attendance")? 'active' : '' ?>" href="<?= BASE_URL ?>attendance">Attendance</a></li>
							<li><a class="departments <?php echo ($last_part == "departments")? 'active' : '' ?>" href="<?= BASE_URL ?>departments">Departments</a></li>
							<li><a class="designations <?php echo ($last_part == "designations")? 'active' : '' ?>" href="<?= BASE_URL ?>designations">Designations</a></li>
						</ul>
					</li>
					<li class="submenu">
						<a href="#"><span> Payroll </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled" style="display: none;">
							<li><a class="<?php echo ($last_part == "payroll")? 'active' : '' ?>" href="<?= BASE_URL ?>payroll"> Employee Salary </a></li>
							<!-- <li><a class="<?php echo ($last_part == "payslip")? 'active' : '' ?>" href="<?=BASE_URL?>payslip/<?=$user_data['employeeid']?>"> Payslip </a></li> -->
						</ul>
					</li>
					<li class="<?php echo ($last_part == "employeesdtrall")? 'active' : '' ?>"> 
						<a class="userslist <?php echo ($last_part == "employeesdtrall")? 'active' : '' ?> <?php echo ($last_part == "individualdtr")? 'active' : '' ?>" href="<?= BASE_URL ?>employeesdtrall">DTR</a>
					</li>
					<li class=" <?php echo ($last_part == "announcements")? 'active' : '' ?>"> 
						<a  class="<?php echo ($last_part == "announcements")? 'active' : '' ?>" href="<?= BASE_URL ?>announcements">Announcements</a>
					</li>
					<li class=" <?php echo ($last_part == "announcements")? 'active' : '' ?>"> 
						<a  class="<?php echo ($last_part == "announcements")? 'active' : '' ?>" href="<?= BASE_URL ?>activities">Logs</a>
					</li>


				<?php } ?>

				<?php if($user_data['role'] == 2 ) { ?>
				
					<li  class="<?php echo ($last_part == "index")? 'active' : '' ?>" > 
						<a href="<?php BASE_URL; ?>index">Dashboard</a>
					</li>
					<li  class="userslist <?php echo ($last_part == "leave")? 'active' : '' ?>"> 
						<a href="<?= BASE_URL ?>leave">Leaves</a>
					</li>
					<li class="<?php echo ($last_part == "userslist")? 'active' : '' ?>"> 
						<a href="<?= BASE_URL ?>userslist">Attendance</a>
					</li>
					<li class="<?php echo ($last_part == "employeesdtr")? 'active' : '' ?>"> 
						<a href="<?= BASE_URL ?>employeesdtr">DTR</a>
					</li>
					<li class="<?php echo ($last_part == "payslip")? 'active' : '' ?>"> 
						<a href="<?= BASE_URL ?>payslip/<?=$user_data['employeeid']?>">Payslip</a>
					</li>

				<?php } ?>


			</ul>
		</div>
    </div>
</div>
<?php } ?>
