<!-- Navigation -->
<?php
	$session = $this->request->session();
	$user_data = $session->read('Auth.User');	
	if(!empty($user_data)) {	
?>
	<!-- <ul class="nav navbar-nav navbar-right user-menu pull-right"> -->
            <li class="dropdown hidden-xs">
                <a href="#" class="hasnotifications dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge bg-purple pull-right">3</span></a>
                <div class="dropdown-menu notifications">
                    <div class="topnav-dropdown-header">
                        <span>Notifications</span>
                    </div>
                    <div class="drop-scroll">
                        <ul class="media-list scroll-content">
                            <li class="media notification-message">
                                <?php foreach ($logs as $log) { ?>
                                 <li class="media notification-message">
                                    <a href="javascript::void(0)">
                                        <div class="media-left">
                                            <span class="avatar">
                                                 <?php foreach($gEmployees as $employee) { ?>
                                                    <?php 
                                                        if($employee->employeeid == $log->userid) {
                                                            echo substr($employee->firstname, 0, 1); 
                                                        }
                                                    ?>
                                                    <?php } ?>
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <p class="m-0 noti-details">
                                                <span class="noti-title">
                                                    <?php foreach($gEmployees as $employee) { ?>
                                                    <?php 
                                                        if($employee->employeeid == $log->userid) {
                                                            echo ($employee->firstname); ?> <?php echo ($employee->lastname); 
                                                        }
                                                    ?>
                                                    <?php } ?>
                                                </span> 
                                                <?=$log->activities;?> in
                                                <span class="noti-title"><?=$log->type;?></span>
                                            </p>
                                            <p class="m-0"><span class="notification-time">6 mins ago</span></p>
                                        </div>
                                    </a>
                                </li>
                                <?php } ?>
                            </li>
                            <!-- <li class="media notification-message">
                                <a href="activities.html">
                                    <div class="media-left">
                                        <span class="avatar">V</span>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0 noti-details"><span class="noti-title">Tarah Shropshire</span> changed the task name <span class="noti-title">Appointment booking with payment gateway</span></p>
                                        <p class="m-0"><span class="notification-time">6 mins ago</span></p>
                                    </div>
                                </a>
                            </li>
                            <li class="media notification-message">
                                <a href="activities.html">
                                    <div class="media-left">
                                        <span class="avatar">L</span>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0 noti-details"><span class="noti-title">Misty Tison</span> added <span class="noti-title">Domenic Houston</span> and <span class="noti-title">Claire Mapes</span> to project <span class="noti-title">Doctor available module</span></p>
                                        <p class="m-0"><span class="notification-time">8 mins ago</span></p>
                                    </div>
                                </a>
                            </li>
                            <li class="media notification-message">
                                <a href="activities.html">
                                    <div class="media-left">
                                        <span class="avatar">G</span>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0 noti-details"><span class="noti-title">Rolland Webber</span> completed task <span class="noti-title">Patient and Doctor video conferencing</span></p>
                                        <p class="m-0"><span class="notification-time">12 mins ago</span></p>
                                    </div>
                                </a>
                            </li>
                            <li class="media notification-message">
                                <a href="activities.html">
                                    <div class="media-left">
                                        <span class="avatar">V</span>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0 noti-details"><span class="noti-title">Bernardo Galaviz</span> added new task <span class="noti-title">Private chat module</span></p>
                                        <p class="m-0"><span class="notification-time">2 days ago</span></p>
                                    </div>
                                </a>
                            </li> -->
                        </ul>
                    </div>
                    <div class="topnav-dropdown-footer">
                        <a href="<?=BASE_URL?>activities">View all Notifications</a>
                    </div>
                </div>
            </li>
            <li class="dropdown">
                <a href="profile.html" class="dropdown-toggle user-link" data-toggle="dropdown" title="Admin">
                    <span class="user-img"><img class="img-circle profilephoto" src="<?= $this->request->webroot; ?>img/user.jpg" width="40" alt="Admin">
                    <span class="status online"></span></span>
                    <span><?php echo ucfirst($user_data['username']); ?></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="<?= BASE_URL ?>profile">My Profile</a></li>
                    <!-- <li><a href="<?=BASE_URL?>editprofile">Edit Profile</a></li>
                    <li><a href="settings.html">Settings</a></li> -->
                    <li><a href="<?= BASE_URL.'logout'?>">Logout</a></li>
                </ul>
            </li>
        <!-- </ul> -->
<?php } ?>