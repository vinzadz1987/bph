<?php
	$session = $this->request->session();
	$user_data = $session->read('Auth.User');
	if(!empty($user_data)) {
?>
<nav>
  <div class="nav toggle">
    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
  </div>

  <ul class="nav navbar-nav navbar-right">
    <li class="">
      <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <img class="profilephoto" src="<?php echo $this->request->webroot; ?>img/default.png" alt=""><?= ucfirst($user_data['firstname']).' '.ucfirst($user_data['lastname']);?>
        <span class=" fa fa-angle-down"></span>
      </a>
      <ul class="dropdown-menu dropdown-usermenu pull-right">
        <li><a href="<?= BASE_URL ?>users/teacheradminupload"> Edit Photo</a></li>
        <li>
			<?php
			echo $this->Html->link(
				$this->Html->tag('i', '', ['class' => 'fa fa-fw fa-sign-out pull-right']) . "Log Out",
				['controller' => 'users', 'action' => 'logout'],
				['class' => 'nav-link','escape' => false]
			);
			?>

        </li>
      </ul>
    </li>
	<li class="">
		<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<span class=" fa fa-calendar"></span> Leave
		</a>
		<ul class="dropdown-menu dropdown-usermenu pull-right">
			<li><a href="<?= BASE_URL ?>users/teacherleavelist"><span class=" fa fa-list"></span> Leave List</a></li>
			<li><a href="<?= BASE_URL ?>users/teacherleaveapplication"><span class=" fa fa-calendar"></span> Leave Form</a></li>
		</ul>
	</li>
    <li role="presentation" class="dropdown">
      <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-envelope-o"></i>
        <span class="badge bg-green"><?php echo $messagecount; ?></span>
      </a>
      <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
        <?php foreach($messagesnew as $message) { ?>
        <!-- <li>
          <a>
            <span class="image"><img class="profilephoto" alt="Profile Image" /></span>
            <span>
              <span>John Smith</span>
              <span class="time">3 mins ago</span>
            </span>
            <span class="message">
              Film festivals used to be do-or-die moments for movie makers. They were where...
            </span>
          </a>
        </li> -->
        <?php } ?>
        <li>
          <div class="text-center">
            <a href="<?= BASE_URL ?>users/teachermessaging">
              <strong>See All Messages</strong>
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </li>
      </ul>
    </li>
    <li role="presentation" class="dropdown">
      <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-bell-o"></i>
        <span class="badge bg-red"><?php echo $messagecount; ?></span>
      </a>
      <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
        <?php foreach($messagesnew as $message) { ?>
        <!-- <li>
          <a>
            <span class="image"><img class="profilephoto" alt="Profile Image" /></span>
            <span>
              <span>John Smith</span>
              <span class="time">3 mins ago</span>
            </span>
            <span class="message">
              Film festivals used to be do-or-die moments for movie makers. They were where...
            </span>
          </a>
        </li> -->
        <?php } ?>
        <li>
          <div class="text-center">
            <a href="<?= BASE_URL ?>users/teachertimeline">
              <strong>See All Logs</strong>
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </li>
      </ul>
    </li>
  </ul>
</nav>
<?php } ?>
