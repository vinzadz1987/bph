<div class="modal-dialog">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<div class="modal-content modal-md">
		<div class="modal-header">
			<h4 class="modal-title">Edit Designation</h4>
		</div>
		<div class="modal-body">
			<?= $this->Form->create($designations,['class' => 'edit_designation_frm']) ?>
				<div class="form-group">
					<label>Designation Name <span class="text-danger">*</span></label>
					<?= $this->Form->control('name', [ 'class' => 'form-control', 'label' => false, 'required' => true ]); ?>
				</div>
				<!-- <div class="form-group">
					<label>Department Name <span class="text-danger">*</span></label>
					<?php echo  $this->Form->select('department', $department, [ 'class' => 'form-control department' ]); ?>
				</div> -->
				<div class="m-t-20 text-center">
					<?= $this->Form->button(__('Create Department'),['class'=>'btn btn-primary']); ?>
				</div>
			<?= $this->Form->end() ?>
		</div>
	</div>
</div>