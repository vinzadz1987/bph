<div id="add_employee" class="modal custom-modal fade" role="dialog">
	<div class="modal-dialog">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<h4 class="modal-title">Working Days as of <?php echo date('01-m-Y').' to '.date('15-m-Y') ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">

					<span class="label label-info-border pull-right ">Number of working days: <?php print_r($countDays[0]->count) ?></span>
					<table class="table table-striped custom-table datatable">
						<thead>
							<tr>
								<th style="width:30%;">Name</th>
								<!-- <th>Employee ID</th> -->
								<th>Time In</th>
								<th>Time Out PM</th>
								<th>Time In PM</th>
								<th>Time Out</th>
								<th>Total Hours</th>
								<th>Date</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($timetracker as $emptime) { ?>
								<tr>
									<td>
										<?php  foreach($employees as $emp) { ?>
											<?php
											if (preg_match("/".$emptime->employeeid."/i",$emp->employeeid)) {
											 	?>
												<a href="javascript::void(0)" class="avatar">
													<?= substr($emp->firstname, 0, 1).''.substr($emp->lastname, 0, 1) ?>
												</a>
												<h2>
												<b> <?= ucfirst($emp->firstname) ?> <?= ucfirst($emp->lastname) ?> <small><?= $emptime->employeeid ?></small></b>
												<span><?= ucfirst($emp->designation->name) ?></span>
												</h2>
											<?php  } ?>
										<?php } ?>
									</td>
									<td><?= $emptime->timein ?></td>
									<td><?= $emptime->timeoutpm ?></td>
									<td><?= $emptime->timeinpm ?></td>
									<td><?= $emptime->timeout ?></td>
									<td><?= $emptime->timediffam ?></td>
									<td><?= $emptime->logdate ?></td>
									<td>
										<?php if($emptime->status == 1){ ?>
											<span class="label label-success-border">Timein</span>
										<?php } else if($emptime->status == 2) { ?>
											<span class="label label-success-border">Timein</span><br>
											<span class="label label-info-border">Timeout PM</span>
										<?php } else if($emptime->status == 3) { ?>
											<span class="label label-success-border">Timein</span><br>
											<span class="label label-info-border">Timeout PM</span><br>
											<span class="label label-warning-border">Timein PM</span>
										<?php } else if($emptime->status == 4) { ?>
											<span class="label label-success-border">Timein</span><br>
											<span class="label label-info-border">Timeout PM</span><br>
											<span class="label label-warning-border">Timein PM</span><br>
											<span class="label label-danger-border">Timeout</span>
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
	</div>
</div>
<a href="#" class="baction hidden" data-toggle="modal" data-target="#add_employee" data-action="edit"><i class="fa fa-pencil m-r-5"></i> Edit</a>
<span class="fa fa-spinner fa-spin spinme" style="font-size:48px; display: none"></span>
<script type="text/javascript">
	$(function () {
		$('.close').click( function() {
			location.href = '<?=BASE_URL?>payroll';
		});
		$(".baction").click();
		if($('baction').click()) {
			$(".spinme").show();
		}
	});
</script>