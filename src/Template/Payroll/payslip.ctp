<div id="add_employee" class="modal custom-modal fade" role="dialog">
	<div class="modal-dialog">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<div class="modal-content modal-lg">
			<div class="modal-body">
				<div class="row">
					<div class="row">
				<div class="col-xs-8">

					<?php 
						date_default_timezone_set('Asia/Manila'); 

						if(date('d') <= 15) {
							if(isset($datefrom) && !empty($datefrom)) {
								$dx = explode('/', $datefrom); 
								$df = $dx[1].'/'.$dx[2].'/'.$dx[0];	
								$dxt = explode('/', $dateto); 
								$dt = $dxt[1].'/'.$dxt[2].'/'.$dxt[0];	
								$range = $df.' - '.$dt;
							} else {
								$range = date('m/01/Y').' - '.date('m/d/Y');
								$saldate =  date('m/15/Y');
							}
						} else {
							if(isset($datefrom) && !empty($datefrom)) {
								$dx = explode('/', $datefrom); 
								$df = $dx[1].'/'.$dx[2].'/'.$dx[0];	
								$dxt = explode('/', $dateto); 
								$dt = $dxt[1].'/'.$dxt[2].'/'.$dxt[0];	
								$range = $df.' - '.$dt;
							} else {
								$range = date('m/15/Y').' - '.date('m/d/Y'); 
								$saldate = date('m/30/Y');
							}
						} 
					?>
					<h4 class="page-title">Payslip of <?php echo $range ?></h4>
				</div>
				<div class="col-sm-4 text-right m-b-30">
					<div class="btn-group btn-group-sm">
						<button class="btn btn-default print"><i class="fa fa-print fa-lg"></i> Print</button>
					</div>
				</div>
			</div>
			<div class="row" id="payslip">
				<div class="col-md-12">
					<div class="card-box">
						<h4 class="payslip-title" style="text-decoration: none">Biliran Provincial Hospital <br> Naval, Biliran</h4>
						<div class="row">
							<div class="col-sm-12">
								<div>
									<table class="table">
										<tbody>
											<tr>
												<td><strong>Name</strong><span class="pull-right"><?= ucfirst($employees[0]->firstname) ?> <?= ucfirst($employees[0]->lastname) ?></span></td>
												<td> <strong>Date</strong><span class="pull-right"><?= $saldate ?></span></td>
											</tr>
											<tr>
												<td> <strong>Designation</strong><span class="pull-right"><?= ucfirst($employees[0]->designation->name) ?></span></td>
												<td> <strong>Department</strong>
													<span class="pull-right">

													<?php foreach ($department as $key => $value) { ?>
														<?php if($value->id == $employees[0]->department) { ?>
															<?php echo $value->name; ?>
														<?php } ?>
													<?php } ?>
														
													</span></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-sm-6">
								<div>
									<h4 class="m-b-10"><strong>Earnings</strong></h4>
									<table class="table table-bordered">
										<tbody>
											<tr>
												<td><strong>Basic Salary</strong> <span class="pull-right"><?= ucfirst($employees[0]->basic_salary) ?></span></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-sm-6">
								<div>
									<h4 class="m-b-10"><strong>Deductions</strong></h4>
									<table class="table table-bordered">
										<tbody>
											<tr>
												<td><strong>Undertime</strong> <span class="pull-right">Php <?=($total_undertime[0]->undertime_total); ?></span></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-12">
								<p><strong>Net Salary: <?php echo 'Php '.floatval( ($employees[0]->daily_rate*$countDays[0]->count) - ($total_undertime[0]->undertime_total) ); ?></strong></p>
							</div>
						</div>
					</div>
				</div>
			</div>
				</div>
			</div>
	</div>
</div>
<a href="#" class="baction hidden" data-toggle="modal" data-target="#add_employee" data-action="edit"><i class="fa fa-pencil m-r-5"></i> Edit</a>
<span class="fa fa-spinner fa-spin spinme" style="font-size:48px; display: none"></span>
<script type="text/javascript">
	$(function () {
		$('.close').click( function() {
			location.href = '<?=BASE_URL?>payroll';
		});
		$(".baction").click();
		if($('baction').click()) {
			$(".spinme").show();
		}
		$(".print").click( function () {
		 	$.print("#payslip" /*, options*/);
		});
	});
</script>
		