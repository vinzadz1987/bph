<div class="page-wrapper">
	<div class="content container-fluid">
	<div class="row">
		<div class="col-xs-4">
			<h4 class="page-title">Employee Daily Time Record</h4>
		</div>
	</div>
	<table class="table table-striped custom-table" id="dtr">
		<thead>
			<tr>
				<th style="width:30%;">Name</th>
				<th>Department</th>
				<th>Designation</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($employees as $emp) { ?>
				<tr>
					<td>
						<a href="javascript::void(0)" class="avatar">
							<?= substr($emp->firstname, 0, 1).''.substr($emp->lastname, 0, 1) ?>
						</a>
						<h2>
							<b> <?= ucfirst($emp->firstname) ?> <?= ucfirst($emp->lastname) ?> <small><?= $emp->employeeid ?></small></b>
						</h2>
					</td>
					<td>
						 <?php foreach ($department as $key => $value) { ?>

						 	<?php if(isset($emp->department)) { ?>
						 		 <?php if ( $emp->department == $value->id ) { ?>
							 		<?= ucfirst($value->name) ?>
							 	<?php } ?> 
						 <?php } ?>
						<?php } ?>
					</td>
					<td><?= ucfirst($emp->designation->name) ?></td>
					<td>
						<?php 
							echo $this->Html->link(
								'<i class="fa fa-calendar" aria-hidden="true"></i> View DTR',
								[
									'controller'=>'employees',
									'action'=>'individualdtr',
									$emp->employeeid
								],
								['escape' => false, 'class' => 'btn btn-success btn-xs ' ]
							);
						?>
						<?php 
							echo $this->Html->link(
								'<i class="fa fa-money" aria-hidden="true"></i> Generate Payslip',
								[
									'controller'=>'payroll',
									'action'=>'payslip',
									$emp->employeeid
								],
								['escape' => false, 'class' => 'btn btn-info btn-xs ' ]
							);
						?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$('#dtr').dataTable( {
		'language': {
			'searchPlaceholder': 'Name, Designation, Department ....'
		}
	});
</script>
