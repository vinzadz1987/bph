
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
        <title>Login - HRMS admin template</title>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
        <link href="https://dreamguys.co.in/hrms/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="https://dreamguys.co.in/hrms/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://dreamguys.co.in/hrms/css/style.css" rel="stylesheet" type="text/css">
        <!--[if lt IE 9]>
            <script src="js//html5shiv.min.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->
    <script type="text/javascript" src="https://gc.kis.v2.scr.kaspersky-labs.com/FC446D2B-E6F5-EF4F-B24D-4F5149EF489E/main.js" charset="UTF-8"></script></head>
    <body>
        <div class="main-wrapper">
            <div class="account-page">
                <div class="container">
                    <h3 class="account-title">Management Login</h3>
                    <div class="account-box">
                        <div class="account-wrapper">
                            <div class="account-logo">
                                <a href="index.html"><img src="images/logo2.png" alt="Focus Technologies"></a>
                            </div>
                            <form action="index.html">
                                <div class="form-group form-focus">
                                    <label class="control-label">Username or Email</label>
                                    <input class="form-control floating" type="text">
                                </div>
                                <div class="form-group form-focus">
                                    <label class="control-label">Password</label>
                                    <input class="form-control floating" type="password">
                                </div>
                                <div class="form-group text-center">
                                    <button class="btn btn-primary btn-block account-btn" type="submit">Login</button>
                                </div>
                                <div class="text-center">
                                    <a href="forgot-password.html">Forgot your password?</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sidebar-overlay" data-reff="#sidebar"></div>
        <script type="text/javascript" src="https://dreamguys.co.in/hrms/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="https://dreamguys.co.in/hrms/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://dreamguys.co.in/hrms/js/app.js"></script>
    </body>
</html>