 <div class="page-wrapper">
	<div class="content container-fluid">
	<div class="row">
		<div class="col-xs-4">
			<h4 class="page-title">Attendance</h4>
		</div>
	</div>
<!-- 	<div class="row filter-row">
		<div class="row filter-row">
			<div class="col-sm-3 col-xs-6">  
				<div class="form-group form-focus">
					<label class="control-label">Employee ID</label>
					<input type="text" class="form-control floating" />
				</div>
			</div>
			<div class="col-sm-3 col-xs-6">  
				<div class="form-group form-focus">
					<label class="control-label">Employee Name</label>
					<input type="text" class="form-control floating" />
				</div>
			</div>
			<div class="col-sm-3 col-xs-6"> 
				<div class="form-group form-focus select-focus">
					<label class="control-label">Designation</label>
					<?php echo  $this->Form->select('designations', $designations, [ 'class' => 'form-control department select floating' ]); ?>
				</div>
			</div>
			<div class="col-sm-3 col-xs-6">  
				<a href="#" class="btn btn-success btn-block"> Search </a>  
			</div>     
		</div>
	</div> -->
	<table class="table table-striped custom-table">
		<thead>
			<tr>
				<th style="width:30%;">Name</th>
				<th>Time In</th>
				<th>Time Out PM</th>
				<th>Time In PM</th>
				<th>Time Out</th>
				<th>Total Hours</th>
				<th>Date</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($timetracker as $emptime) { ?>
				<tr>
					<td>
						<?php  foreach($employees as $emp) { ?>
							<?php
							if (preg_match("/".$emptime->employeeid."/i",$emp->employeeid)) {
							 	?>
								<a href="javascript::void(0)" class="avatar">
									<?= substr($emp->firstname, 0, 1).''.substr($emp->lastname, 0, 1) ?>
								</a>
								<h2>
								<b> <?= ucfirst($emp->firstname) ?> <?= ucfirst($emp->lastname) ?> <small><?= $emptime->employeeid ?></small></b>
								<span><?= ucfirst($emp->designation->name) ?></span>
								</h2>
							<?php  } ?>
						<?php } ?>
					</td>
					<td><?= $emptime->timein ?></td>
					<td><?= $emptime->timeoutpm ?></td>
					<td><?= $emptime->timeinpm ?></td>
					<td><?= $emptime->timeout ?></td>
					<td><?= $emptime->timediffam ?></td>
					<td><?= $emptime->logdate ?></td>
					<td>
						<?php if($emptime->status == 1){ ?>
							<span class="label label-success-border">Timein</span>
						<?php } else if($emptime->status == 2) { ?>
							<span class="label label-success-border">Timein</span><br>
							<span class="label label-info-border">Timeout PM</span>
						<?php } else if($emptime->status == 3) { ?>
							<span class="label label-success-border">Timein</span><br>
							<span class="label label-info-border">Timeout PM</span><br>
							<span class="label label-warning-border">Timein PM</span>
						<?php } else if($emptime->status == 4) { ?>
							<span class="label label-success-border">Timein</span><br>
							<span class="label label-info-border">Timeout PM</span><br>
							<span class="label label-warning-border">Timein PM</span><br>
							<span class="label label-danger-border">Timeout</span>
						<?php } ?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$('.table').dataTable( {
		'language': {
			'searchPlaceholder': 'Name, Time In, Time out ....'
		}
	});
</script>



    <!--  <div class="main-wrapper">
            <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">Attendance Sheet</h4>
						</div>
					</div>
					<div class="row filter-row">
						<div class="col-sm-3 col-xs-6">  
							<div class="form-group form-focus">
								<label class="control-label">Employee Name</label>
								<input type="text" class="form-control floating" />
							</div>
						</div>
						<div class="col-sm-3 col-xs-6"> 
							<div class="form-group form-focus select-focus">
								<label class="control-label">Select Month</label>
								<?php $months = ['-','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'] ?>
								<select class="select floating" id="selectMonths"> 
									<?php for($i=0;$i<=12; $i++) { ?>
										<?php if( $i == date('m') ) { ?>
											<option value="<?= $i ?>" selected><?= $months[$i] ?></option>
										<?php } else { ?>
											<option value="<?= $i ?>"><?= $months[$i] ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-sm-3 col-xs-6"> 
							<div class="form-group form-focus select-focus">
								<label class="control-label">Select Year</label>
								<select class="select floating" id="selectYears"> 
									<option>-</option>
									<option value="2018" selected>2018</option>
									<option value="2017">2017</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3 col-xs-6">  
							<a href="#" class="btn btn-success btn-block"> Search </a>  
						</div>     
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
							<div class="table-responsive"> -->
								<!-- <table class="table table-striped custom-table m-b-0">
									<thead>
										<tr>
											<th>Employee</th>
											<th>1</th>
											<th>2</th>
											<th>3</th>
											<th>4</th>
											<th>5</th>
											<th>6</th>
											<th>7</th>
											<th>8</th>
											<th>9</th>
											<th>10</th>
											<th>11</th>
											<th>12</th>
											<th>13</th>
											<th>14</th>
											<th>15</th>
											<th>16</th>
											<th>17</th>
											<th>18</th>
											<th>19</th>
											<th>20</th>
											<th>22</th>
											<th>23</th>
											<th>24</th>
											<th>25</th>
											<th>26</th>
											<th>27</th>
											<th>28</th>
											<th>29</th>
											<th>30</th>
											<th>31</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>John Doe</td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><div class="half-day"><span class="first-off"><i class="fa fa-check text-success"></i></span> <span class="first-off"><i class="fa fa-close text-danger"></i></span></div></td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-close text-danger"></i> </td>
											<td><i class="fa fa-close text-danger"></i> </td>
											<td><i class="fa fa-close text-danger"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-close text-danger"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><div class="half-day"><span class="first-off"><i class="fa fa-close text-danger"></i></span> <span class="first-off"><i class="fa fa-check text-success"></i></span></div></td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-close text-danger"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-close text-danger"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
											<td><i class="fa fa-check text-success"></i> </td>
										</tr>
									S
									</tbody>
								</table> -->
               					<!-- <div><table id="placeholder" class="table table-striped custom-table m-b-0"></table></div>
               					<div id="tableAttendance"></div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">

			$(function() {


				var monthDayCount = null;
				$("#selectMonths").change( function() {
					monthDayCount = getDaysInMonth($(this).val(), $("#selectYears").val());
					$("#placeholder").html(MakeTablejQuery(5, monthDayCount+1));
					getSatSunofTheMonth(monthDayCount);
				});
				$("#selectYears").change( function() {
					monthDayCount = getDaysInMonth($("#selectMonths").val(), $(this).val());
					$("#placeholder").html(MakeTablejQuery(5, monthDayCount+1));
					getSatSunofTheMonth(monthDayCount);
				});

				monthDayCount = getDaysInMonth($("#selectMonths").val(), $("#selectYears").val());
				$("#placeholder").html(MakeTablejQuery(5, monthDayCount+1));

			});

			function getDaysInMonth(month, year) {
				return ((new Date (year, month)).getTime() - (new Date (year, month-1)).getTime())/(1000*60*60*24);
			}

			function getSatSunofTheMonth(monthDayCount) {

				var d = new Date();
				var sat = new Array();
				var sun = new Array();

				for(var i=1;i<=monthDayCount;i++){
					var newDate = new Date(d.getFullYear(),d.getMonth(),i)
					console.log(i+"-"+newDate.getDay());
					if(newDate.getDay()==0){
						sat.push(i);
					}
					if(newDate.getDay()==6){
						sun.push(i);
					}

				}
				console.log(sat);
				console.log(sun);


			}

			Number.prototype.times = function(fn) {
				for(var r = [], i = 0; i < this; i++)
					r.push(fn(i));
				return r;
			};

			function MakeTablejQuery(numRows, numCols) {

				var header = numCols.times(function(c) {
					if(c == 0) {
						return $("<th/>").text("Employee");
					} else {
						return $("<th/>").text(c);
					}
				});

				var row = function(r) {
					return $("<tr/>").append(numCols.times(function(c) {
					return $("<td/>").text([c, r].join(""));
					}));
				};

				// create table
				var $table = $('<table class="table table-striped custom-table m-b-0">');
				// caption
				$table.append('<thead>').children('thead')
				.append('<tr />').children('tr').append(header);

				//tbody
				var $tbody = $table.append('<tbody />').children('tbody');

				// add row
				$tbody.append('<tr />').children('tr:last')
				.append("<td>val</td>")
				.append("<td>val</td>")
				.append("<td>val</td>")
				.append("<td>val</td>");

				// add another row
				$tbody.append('<tr />').children('tr:last')
				.append("<td>val</td>")
				.append("<td>val</td>")
				.append("<td>val</td>")
				.append("<td>val</td>");

				// add table to dom
				return $table.appendTo('#tableAttendance');

			}

        </script> -->