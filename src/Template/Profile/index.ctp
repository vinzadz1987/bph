
        <div class="main-wrapper">
            <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">My Profile</h4>
						</div>
						
						<div class="col-sm-4 text-right m-b-30">
							<?php $s = $this->request->session()->read('Auth.User'); ?>
							<a href="<?=BASE_URL?>editprofile/<?=$s['id']?>" class="btn btn-primary rounded"><i class="fa fa-plus"></i> Edit Profile</a>
						</div>
					</div>
					<div class="card-box">
						<div class="row">
							<div class="col-md-12">
								<div class="profile-view">
									<div class="profile-img-wrap">
										<div class="profile-img">
											<a href="#"><img class="avatar" src="images/user.jpg" alt=""></a>
										</div>
									</div>
									<div class="profile-basic">
										<div class="row">
											<div class="col-md-5">
												<div class="profile-info-left">
													<h3 class="user-name m-t-0 m-b-0"><?=ucfirst($employees[0]->firstname)?> <?=ucfirst($employees[0]->lastname)?></h3>
													<small class="text-muted"><?=ucfirst($employees[0]->designation->name)?> </small>
													<div class="staff-id">Employee ID :<?=ucfirst($employees[0]->employeeid)?></div>
													<!-- <div class="staff-msg"><a href="chat.html" class="btn btn-custom">Send Message</a></div> -->
												</div>
											</div>
											<div class="col-md-7">
												<ul class="personal-info">
													<li>
														<span class="title">Phone:</span>
														<span class="text"><a href=""><?=$employees[0]->phone?></a></span>
													</li>
													<li>
														<span class="title">Email:</span>
														<span class="text"><a href=""><?=$employees[0]->email?></a></span>
													</li>
													<li>
														<span class="title">Birth Place:</span>
														<span class="text"><?=$employees[0]->place_of_birth?></span>
													</li>
													<li>
														<span class="title">Birthday:</span>
														<span class="text"><?=$employees[0]->birthdate?></span>
													</li>
													<li>
														<span class="title">Civil Status:</span>
														<span class="text"><?=$employees[0]->civilstatus?></span>
													</li>
													<li>
														<span class="title">Gender:</span>
														<span class="text"><?=$employees[0]->male?></span>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<!-- <div class="col-md-3">
							<div class="card-box m-b-0">
								<h3 class="card-title">Skills</h3>
								<div class="skills">
									<span>IOS</span>
									<span>Android</span> 
									<span>Html</span>
									<span>CSS</span>
									<span>Codignitor</span>
									<span>Php</span>
									<span>Javascript</span>
									<span>Wordpress</span>
									<span>Jquery</span>
								</div>
							</div>
						</div> -->
						<div class="col-md-12">
							<div class="card-box">
								<h3 class="card-title">Promotions</h3>
								<div class="experience-box">
									<ul class="experience-list">
										<li>
											<div class="experience-user">
												<div class="before-circle"></div>
											</div>
											<div class="experience-content">
												<div class="timeline-content">
													<a href="#/" class="name">Test</a>
													<div>Venue</div>
													<span class="time">Date</span>
												</div>
											</div>
										</li>
										<li>
											<div class="experience-user">
												<div class="before-circle"></div>
											</div>
											<div class="experience-content">
												<div class="timeline-content">
													<a href="#/" class="name">Test</a>
													<div>Venue</div>
													<span class="time">Date</span>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="card-box m-b-0">
								<h3 class="card-title">Trainings</h3>
								<div class="experience-box">
									<ul class="experience-list">
										<li>
											<div class="experience-user">
												<div class="before-circle"></div>
											</div>
											<div class="experience-content">
												<div class="timeline-content">
													<a href="#/" class="name">Web Designer at Zen Corporation</a>
													<span class="time">Jan 2013 - Present (5 years 2 months)</span>
												</div>
											</div>
										</li>
										<li>
											<div class="experience-user">
												<div class="before-circle"></div>
											</div>
											<div class="experience-content">
												<div class="timeline-content">
													<a href="#/" class="name">Web Designer at Ron-tech</a>
													<span class="time">Jan 2013 - Present (5 years 2 months)</span>
												</div>
											</div>
										</li>
										<li>
											<div class="experience-user">
												<div class="before-circle"></div>
											</div>
											<div class="experience-content">
												<div class="timeline-content">
													<a href="#/" class="name">Web Designer at Dalt Technology</a>
													<span class="time">Jan 2013 - Present (5 years 2 months)</span>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>