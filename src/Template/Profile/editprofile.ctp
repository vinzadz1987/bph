
        <div class="main-wrapper">
            <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">Edit Profile</h4>
						</div>
					</div>
					<?= $this->Flash->render() ?>
					<!-- <form> -->
						<div class="card-box">
							<h3 class="card-title">Basic Informations</h3>
							<div class="row">
								<div class="col-md-12">
									<!-- <?php var_dump($editprofile); ?> -->
									<?= $this->Form->create($editprofile,['enctype' =>'multipart/form-data']) ?>
									<div class="profile-img-wrap">
										<img class="inline-block mb-10 profilephoto" src="<?php echo $this->request->webroot; ?>img/default.png" alt="user">
										<div class="fileupload btn btn-default">
											<a href="<?=BASE_URL?>/uploadphoto"><span class="btn-text">edit</span></a>
											<?= $this->Form->control('photo', [  'class' => 'upload', 'label' => false, 'type' => 'file' ]); ?>
										</div>
									</div>
									<div class="profile-basic">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group form-focus">
													<div class="col-xs-12">
													<label class="control-label" style="padding-left: 10px">First Name</label>
													<!-- <input type="text" class="form-control floating" /> -->
													<?= $this->Form->control('firstname', [  'class' => 'form-control floating', 'label' => false ]); ?>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group form-focus">
													<div class="col-xs-12">
														<label class="control-label" style="padding-left: 10px">Last Name</label>
														<!-- <input type="text" class="form-control " /> -->
														<?= $this->Form->control('lastname', [  'class' => 'form-control floating', 'label' => false  ]); ?>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group form-focus">
													<div class="col-xs-12">
														<label class="control-label" style="padding-left: 10px">Birth Date</label>
														<!-- <div class="cal-icon"><input class="form-control floating datetimepicker" type="text"></div> -->
														<?= $this->Form->control('birthdate', [  'class' => 'form-control datetimepicker floating', 'label' => false  ]); ?>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group form-focus select-focus">
													<div class="col-xs-12">
													<label class="control-label" style="padding-left: 10px">Gender</label>
													<!-- <select class="select form-control floating">

														<option value="">Select Gendar</option>
														<option value="">Male</option>
														<option value="">Female</option>
													</select> -->
														<?php 
															$sex = [
																		'' => 'Select Gender',
																		'Male' => 'Male',
																		'Female' => 'Female'
																	];
															echo $this->Form->select('sex', $sex, ['default' => '', 'class' => 'form-control select floating', 'label' => false]); 
														?>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group form-focus select-focus">
													<div class="col-xs-12">
													<label class="control-label" style="padding-left: 10px">Department</label>
														<?php echo  $this->Form->select('department', $department, [ 'class' => 'form-control department' ]); ?>
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group form-focus select-focus">
													<div class="col-xs-12">
													<label class="control-label" style="padding-left: 10px">Designation</label>
														<?php echo  $this->Form->select('designation', $designations, [ 'class' => 'form-control department' ]); ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card-box">
							<h3 class="card-title">Contact Informations</h3>
							<div class="row">
								<!-- <div class="col-md-12">
									<div class="form-group form-focus">
										<label class="control-label">Address</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">State</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Country</label>
										<input type="text" class="form-control floating" />
									</div>
								</div> -->
								<div class="col-md-6">
									<div class="form-group form-focus">
										<div class="col-xs-12">
											<label class="control-label" style="padding-left: 10px">Pin Code</label>
											<!-- <input type="text" class="form-control floating" /> -->
											<?= $this->Form->control('pincode', [  'class' => 'form-control floating','type' => 'number', 'label' => false]); ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<div class="col-xs-12">
											<label class="control-label" style="padding-left: 10px">Phone Number</label>
											<!-- <input type="text" class="form-control floating" /> -->
											<?= $this->Form->control('phone', [  'class' => 'form-control floating', 'label' => false ]); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="card-box">
							<h3 class="card-title">Education Informations</h3>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Institution</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Subject</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Starting Date</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Complete Date</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Degree</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Grade</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
							</div>
							<div class="add-more">
								<a href="#" class="btn btn-primary"><i class="fa fa-plus"></i> Add More Institute</a>
							</div>
						</div> -->
						<!-- <div class="card-box">
							<h3 class="card-title">Experience Informations</h3>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Company Name</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Location</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Job Position</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Period From</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="control-label">Period To</label>
										<input type="text" class="form-control floating" />
									</div>
								</div>
							</div>
							<div class="add-more">
								<a href="#" class="btn btn-primary"><i class="fa fa-plus"></i> Add More Experience</a>
							</div>
						</div> -->
						<div class="text-center m-t-20">
							<?= $this->Form->button(__('Save & Update'),['class'=>'btn btn-primary btn-lg'] ); ?>
							<!-- <button class="btn btn-primary btn-lg" type="button">Save &amp; update</button> -->
						</div>
						<?= $this->Form->end() ?>
					<!-- </form> -->
				</div>
			</div>
		</div>