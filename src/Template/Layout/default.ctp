<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>


<!DOCTYPE html>
<html>
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
		<title><?= $this->fetch('title') ?></title>

		<?= $this->Html->css([
			'bootstrap.min',
			'select2.min',
			'dataTables.bootstrap4.min'
			// 'cake'
		]);?>
		<link rel="stylesheet" href="https://dreamguys.co.in/hrms/css/bootstrap-datetimepicker.min.css" type="text/css">
		<link href="https://dreamguys.co.in/hrms/css/style.css" rel="stylesheet" type="text/css">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<?= $this->Html->charset() ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
		<?= $cakeDescription ?>:
		<?= $this->fetch('title') ?>
		</title>
		<?= $this->Html->meta('icon') ?>
		<?= $this->fetch('meta') ?>
		<?= $this->fetch('css') ?>
		<?= $this->fetch('script') ?>

		<link rel="stylesheet" href="https://dreamguys.co.in/hrms/css/select2.min.css" type="text/css">
		<link rel="stylesheet" href="https://dreamguys.co.in/hrms/css/bootstrap-datetimepicker.min.css" type="text/css">
		<link href="https://dreamguys.co.in/hrms/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />


		<?= $this->Html->script([
			'jquery-3.2.1.min.js',
			'jquery.maskedinput' 
		]);?>

		<?= $this->Html->script([
			'datatables.net/js/jquery.dataTables.min',
			'datatables.net-bs/js/dataTables.bootstrap.min',
			'datatables.net-buttons/js/dataTables.buttons.min',
			'datatables.net-buttons-bs/js/buttons.bootstrap.min',
			'datatables.net-buttons/js/buttons.flash.min',
			'datatables.net-buttons/js/buttons.html5.min',
			'datatables.net-buttons/js/buttons.print.min',
			'datatables.net-responsive/js/dataTables.responsive.min',
			'datatables.net-fixedheader/js/dataTables.fixedHeader.min',
			'jQuery.print',
		]);?>


   	 	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
		
</head>
<style type="text/css">
	

div.message {
    text-align: center;
    cursor: pointer;
    display: block;
    font-weight: normal;
    padding: 0 1.5rem 0 1.5rem;
    transition: height 300ms ease-out 0s;
    background-color: #a0d3e8;
    color: #626262;
    top: 15px;
    right: 15px;
    z-index: 999;
    overflow: hidden;
    height: 50px;
    line-height: 2.5em;
    box-radius: 5px;
    padding-top: 5px;
    margin-bottom: 5px;
}

div.message:before {
    line-height: 0px;
    font-size: 20px;
    height: 12px;
    width: 12px;
    border-radius: 15px;
    text-align: center;
    vertical-align: middle;
    display: inline-block;
    position: relative;
    left: -11px;
    background-color: #FFF;
    padding: 12px 14px 12px 10px;
    content: "i";
    color: #a0d3e8;
}

div.message.error {
    background-color: #C3232D;
    color: #FFF;
}

div.message.error:before {
    padding: 11px 16px 14px 7px;
    color: #C3232D;
    content: "x";
}
div.message.hidden {
    height: 0;
}
.noti-dot:before {content: '';width: 5px;height: 5px;border: 5px solid #ff9b44 ;-webkit-border-radius: 30px;-moz-border-radius: 30px;border-radius: 30px;background-color: #ff9b44 ;z-index: 10;position: absolute;right: 37px;top: 17px;}
.noti-dot:after {
	content: '';
	border: 4px solid #ff9b44 ;
	background: transparent;
	-webkit-border-radius: 60px;
	-moz-border-radius: 60px;
	border-radius: 60px;
	height: 24px;
	width: 24px;
	-webkit-animation: pulse 3s ease-out;
	-moz-animation: pulse 3s ease-out;
	animation: pulse 3s ease-out;
	-webkit-animation-iteration-count: infinite;
	-moz-animation-iteration-count: infinite;
	animation-iteration-count: infinite;
	position: absolute;
	top: 10px;
	right: 30px;
	z-index: 1;
	opacity: 0;
}
</style>
<body>
	<?php
		$session = $this->request->session();
		$user_data = $session->read('Auth.User');
		if(!empty($user_data)) {
	?>
	<div class="header">
		<div class="header-left">
			<a href="index.html" class="logo">
				<!-- <img src="images/logo.png" width="40" height="40" alt=""> -->
			</a>
			<a href="<?= BASE_URL ?>" class="logo-dark">
				<img src="<?= $this->request->webroot; ?>img/bphlogo.png" width="40" height="40" alt="">
			</a>
		</div>
		<div class="page-title-box pull-left">
			<h3>BILIRAN PROVINCIAL HOSPITAL</h3>
		</div>
		<a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
		<ul class="nav navbar-nav navbar-right user-menu pull-right">
			<?= $this->element('meta/navbar') ?>
		</ul>
		<div class="dropdown mobile-user-menu pull-right">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
			<ul class="dropdown-menu pull-right">
				<li><a href="<?=BASE_URL?>profile">My Profile</a></li>
				<li><a href="edit-profile.html">Edit Profile</a></li>
				<li><a href="settings.html">Settings</a></li>
				<li><a href="login.html">Logout</a></li>
			</ul>
		</div>
	</div>
	<?php } ?>
    <?= $this->Flash->render() ?>
    <?= $this->element('meta/sidebar') ?>
    <?= $this->fetch('content') ?>
    <footer>
    </footer>
	
    <script type="text/javascript">
    	var dir = photo = "";
    	if('<?php echo $usersphoto[0]->photo; ?>' != "") {
    		photo = '<?php echo $usersphoto[0]->photo; ?>';
    		dir = "profile_photo";
    	} else {
    		photo = 'default.png';
    	}

		$(".profilephoto").attr('src','<?php echo $this->request->webroot; ?>img/'+dir+'/'+photo);
	</script>
	
	<div class="sidebar-overlay" data-reff="#sidebar"></div>
	<script type="text/javascript" src="https://dreamguys.co.in/hrms/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://dreamguys.co.in/hrms/js/jquery.slimscroll.js"></script>
	<script type="text/javascript" src="https://dreamguys.co.in/hrms/js/select2.min.js"></script>
	<script type="text/javascript" src="https://dreamguys.co.in/hrms/js/moment.min.js"></script>
	<?= $this->Html->script([
		'app.js',
		'jquery.dataTables.min',
		'dataTables.bootstrap4',
		'bootstrap-datetimepicker.min' 
	]);?>
	</body>
</html>
