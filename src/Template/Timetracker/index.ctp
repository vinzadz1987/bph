<style type="text/css">
	.time-frame {
		background-color: #1f1717;
		color: #ffffff;
		font-family: Arial;
		padding: 34px;
		font-size: 19px;
		height: 165px;
	}

	.time-frame > div {
		width: 100%;
		text-align: center;
	}

	#date-part {
		font-size: 1.2em;
	}
	#time-part {
		font-size: 3em;
	}
</style>
<div style="padding: 5%;">
<div class="card-box">
		<div class="row">
			<div class="col-md-12">
				<div class="profile-view">
					<div>
						<div class="row">
							<div class="col-md-7">
								<div class="profile-info-left">
								    <div class="content container-fluid">
										<div class="row">
											<div class="col-md-12">
												<div class="table-responsive">
													<a href="<?=BASE_URL?>" class="pull-right"><span class="fa fa-home"></span></a>
													<table class="table table-striped custom-table datatable">
														<thead>
															<tr>
																<th style="width:30%;">Name</th>
																<!-- <th>Employee ID</th> -->
																<th>Time In</th>
																<th>Time Out PM</th>
																<th>Time In PM</th>
																<th>Time Out</th>
																<th>Date</th>
																<th>Status</th>
															</tr>
														</thead>
														<tbody>
															<?php foreach($timetracker as $emptime) { ?>
																<tr>
																	<td>
																		<?php  foreach($employees as $emp) { ?>
																			<?php
																			if (preg_match("/".$emptime->employeeid."/i",$emp->employeeid)) {
																			 	?>
																				<a href="javascript::void(0)" class="avatar">
																					<?= substr($emp->firstname, 0, 1).''.substr($emp->lastname, 0, 1) ?>
																				</a>
																				<h2>
																				<b> <?= ucfirst($emp->firstname) ?> <?= ucfirst($emp->lastname) ?> <small><?= $emptime->employeeid ?></small></b>
																				<span><?= ucfirst($emp->designation->name) ?></span>
																				</h2>
																			<?php  } ?>
																		<?php } ?>
																	</td>
																	<td><?= $emptime->timein ?></td>
																	<td><?= $emptime->timeoutpm ?></td>
																	<td><?= $emptime->timeinpm ?></td>
																	<td><?= $emptime->timeout ?></td>
																	<td><?= $emptime->logdate ?></td>
																	<td>
																		<?php if($emptime->status == 1){ ?>
																			<span class="label label-success-border">Timein</span>
																		<?php } else if($emptime->status == 2) { ?>
																			<span class="label label-success-border">Timein</span><br>
																			<span class="label label-info-border">Timeout PM</span>
																		<?php } else if($emptime->status == 3) { ?>
																			<span class="label label-success-border">Timein</span><br>
																			<span class="label label-info-border">Timeout PM</span><br>
																			<span class="label label-warning-border">Timein PM</span>
																		<?php } else if($emptime->status == 4) { ?>
																			<span class="label label-success-border">Timein</span><br>
																			<span class="label label-info-border">Timeout PM</span><br>
																			<span class="label label-warning-border">Timein PM</span><br>
																			<span class="label label-danger-border">Timeout</span>
																		<?php } ?>
																	</td>
																</tr>
															<?php } ?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
								    </div>
								</div>
							</div>
							<div class="col-md-5">
								<div class='time-frame col-sm-12'>
									<div class="col-sm-3 col-xs-6" id='date-part'></div>
									<div class="col-sm-3 col-xs-6" id='time-part'></div>
								</div>
								<div class="col-sm-12" style="margin-top: 25px">
									<?= $this->Form->create($timetracker,['url' => '/timetracker/addtimelog']) ?>		
									<div class="form-group">
										<?= $this->Form->control('pincode', [  'class' => 'form-control', 'label' => false, 'type' => 'number', 'placeholder' => 'Enter Pincode']); ?>
										<?= $this->Form->control('time', [  'type' => 'hide']); ?>
									</div>
									<?= $this->Form->button(__('Create Employee'),['class'=>'btn btn-primary hide'] ); ?>
									<?= $this->Form->end() ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://dreamguys.co.in/hrms/js/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
	$(function () {
		$("#pincode").val("");
		$("#pincode").focus("");
		var interval = setInterval( function () {
			var momentNow = moment();
			$('#date-part').html(momentNow.format('YYYY MMMM DD') + ' '
				+ momentNow.format('dddd')
				.substring(0,3).toUpperCase());
			$('#time-part').html(momentNow.format('A hh:mm:ss'));
			$("#time").val(momentNow.format('h:m:s A'));
		}, 100);
	});
</script>










<!-- 
 <!DOCTYPE html>
  <meta charset="utf-8" />
  <title>WebSocket Test</title>
  <script language="javascript" type="text/javascript">

  var wsUri = "ws://echo.websocket.org/";
  var output;

  function init()
  {
    output = document.getElementById("output");
    testWebSocket();
  }

  function testWebSocket()
  {
    websocket = new WebSocket(wsUri);
    websocket.onopen = function(evt) { onOpen(evt) };
    websocket.onclose = function(evt) { onClose(evt) };
    websocket.onmessage = function(evt) { onMessage(evt) };
    websocket.onerror = function(evt) { onError(evt) };
  }

  function onOpen(evt)
  {
    writeToScreen("CONNECTED");
    doSend("WebSocket rocks");
  }

  function onClose(evt)
  {
    writeToScreen("DISCONNECTED");
  }

  function onMessage(evt)
  {
    writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
    websocket.close();
  }

  function onError(evt)
  {
    writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
  }

  function doSend(message)
  {
    writeToScreen("SENT: " + message);
    websocket.send(message);
  }

  function writeToScreen(message)
  {
    var pre = document.createElement("p");
    pre.style.wordWrap = "break-word";
    pre.innerHTML = message;
    output.appendChild(pre);
  }

  window.addEventListener("load", init, false);

  </script>

  <h2>WebSocket Test</h2>

  <div id="output"></div> -->