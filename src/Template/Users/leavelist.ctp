<div class="page-wrapper">
    <div class="content container-fluid">
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Leave List</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table id="leaveTable" class="table table-striped custom-table table-bordered">
						<thead>
							<tr>
								<th>ID Number</th>
								<th>Name</th>
								<th>Reason</th>
								<th>Leave Date</th>
								<th>Status</th>
								<th style="width: 145px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($leaves as $leave): ?>
							<tr>
								<td><?=$leave->userid ?></td>
								<td>
									<?php foreach($employees as $emp) { ?>
										<?php if($emp->employeeid == $leave->userid) {?>
											<?php echo $emp->firstname ?> <?php echo $emp->lastname ?>
										<?php } ?>
									<?php } ?>
								</td>
								<td><?=h($leave->leave_reason)?></td>
								<td><?=h($leave->leave_date)?></td>
								<td>
									<?php 
										if ($leave->leave_approval == 0) {
											echo '<span class="label label-default">pending</span>';
										} else if ($leave->leave_approval == 1) {
											echo '<span class="label label-success">approved</span>';
										} else {
											echo '<span class="label label-danger">rejected</span>';
										}
									?>
								</td>
								<td>
									<button class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal" onclick="getLeave('<?php echo $leave->id ?>',1)"> <i class="fa fa-check"></i> Approve</button>
									<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal" onclick="getLeave('<?php echo $leave->id ?>',2)"> <i class="fa fa-times"></i> Reject</button>
								</td>
							</tr>
							<?php endforeach; ?> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog" style="width: 330px;">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				<p>
					<div id="checkboxes">
						<input type="hidden" id="leavid">
						<input type="hidden" id="type">
						<h4> Are you sure you want to <span id="typename"></span> this leave?
					</div>
          
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="submitCheck">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript">
	$(function() {
		$("#submitCheck").on('click', function() {
			$.ajax({
				type: 'POST',
				url:'<?=BASE_URL?>leaveactions/'+$("#leavid").val(),
				data: { leave_approval: $("#type").val() },
				success : function(data) {
					location.reload();
				}
			});
		});
		$('#leaveTable').dataTable( {
			'language': {
				'searchPlaceholder': 'ID Number, Name, Reason ....'
			}
		});
	});
	function getLeave(id,type) {
		if(type == 1) {
			$('#typename').text("Approve");
		} else {
			$('#typename').text("Reject");
		}
		$("#leavid").val(id);
		$("#type").val(type);
	}
</script>

