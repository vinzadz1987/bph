
        <div class="main-wrapper">
            <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h4 class="page-title">Designations</h4>
						</div>
						<div class="col-sm-4 text-right m-b-30">
							<a href="#" class="btn btn-primary rounded" data-toggle="modal" data-target="#add_designation"><i class="fa fa-plus"></i> Add New Designation</a>
						</div>
					</div>
					<?= $this->Flash->render() ?>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-striped custom-table m-b-0 datatable">
									<thead>
										<tr>
											<th>#</th>
											<th>Designation </th>
											<th class="text-right">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($designations as $key => $desig) { ?>
											<tr>
												<td><?= $key ?></td>
												<td><?= $desig->name ?></td>
												<td class="text-right">
													<div class="dropdown">
														<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
														<ul class="dropdown-menu pull-right">
															<li><a  href="editdesignations/<?= $desig->id ?>" class="edit_designation" data-value='<?= $desig->name ?>' data-toggle="modal" data-target="#edit_designation" title="Edit"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
															<li>
																<?php echo $this->Html->link(
																	'<i class="fa fa-trash-o m-r-5"></i> Delete',
																	['controller' => 'Users', 'action' => 'deletedesignations', $desig->id],
																	['confirm' => 'Are you sure you wish to delete this designation?', 'escape' => false]
																); ?>
															</li>
														</ul>
													</div>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
				<div class="notification-box">
					<?= $this->element('include/notification'); ?>
				</div>
            </div>
			<div id="delete_designation" class="modal custom-modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content modal-md">
						<div class="modal-header">
							<h4 class="modal-title">Delete Designation</h4>
						</div>
						<div class="modal-body card-box">
							<p>Are you sure want to delete this?</p>
							<div class="m-t-20 text-left">
								<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
								<button type="submit" class="btn btn-danger">Delete</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="add_designation" class="modal custom-modal fade" role="dialog">
				<?= $this->element('modal/add_designation') ?>
			</div>
			<div id="edit_designation" class="modal custom-modal fade" role="dialog">
				<?= $this->element('modal/edit_designation') ?>
			</div>
        </div>

<script type="text/javascript">

	$(function () {

		$('.edit_designation').click( function() {
			var href = $(this).attr('href');
			$(".edit_designation_frm").attr('action',href);
			$('.edit_designation_frm').find("#name").val($(this).attr('data-value').split('_')[0]);
			$('.department').select2('val', $(this).attr('data-value').split('_')[1]);
		});

		$('.department').select2({
			placeholder:'Enter Designation',
			allowClear: true,
			width: 500
		});
	});

</script>