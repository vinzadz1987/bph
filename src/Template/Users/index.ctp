 <div class="page-wrapper">
 <div class="content container-fluid">
    <?php foreach ($announcements as $key => $value) { ?>
      <div class="col-lg-3 col-sm-4">
        <div class="card-box project-box">
          <h4 class="project-title"><a href="project-view.html"><?=ucfirst($value->name)?></a></h4>
          <small class="block text-ellipsis m-b-15">
            <span class="text-xs">Venue: </span> <span class="text-muted"><?=ucfirst($value->venue)?></span>
          </small>
          <p class="text-muted">
            <?=ucfirst($value->description)?>
          </p>
          <div class="pro-deadline m-b-15">
            <div class="sub-title">
              Date:
            </div>
            <div class="text-muted">
              <?=$value->date?>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
</div>
</div>