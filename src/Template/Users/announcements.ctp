<div class="page-wrapper">
	<div class="content container-fluid">
	<div class="">
	<div class="row">
		<div class="col-xs-4">
			<h4 class="page-title">Announcement</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<!-- <h2>Anouncements</h2> -->
					<div class="pull-right">
						<div class="input-group">
							<?php
								echo $this->Html->link(
									'<i class="fa fa-plus m-r-5"></i> Add Event',
									[
									'controller' => 'users',
									'action' => 'addannouncements'
									],
									[ 'escape' => false, 'class' => 'btn btn-primary rounded pull-right baction' ]
								)
							?>
						</div>
					</div>
					<div class="clearfix"></div><br>
				</div>
				<div class="x_content">
					<table id="events" class="table table-striped table-bordered custom-table">
						<thead>
							<tr>
								<th>Title</th>
								<th>Description</th>
								<th>Venue</th>
								<th>Date</th>
								<th style="width: 145px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($announcements as $ts): ?>
							<tr>
								<td><?=$ts->name ?></td>
								<td><?=$ts->description; ?> </td>
								<td><?=$ts->venue; ?> </td>
								<td><?=h($ts->date)?></td>
								<td>
									<?php 
										echo $this->Html->link(
										'<i class="fa fa-pencil" aria-hidden="true"></i>',
										[
											'controller'=>'users',
											'action'=>'editannouncements',
											h($ts->id)
										],
										['escape' => false, 'class' => 'btn btn-success btn-xs ' ]
										);
									?>
									<?php 
										echo $this->Html->link(
										'<i class="fa fa-times" aria-hidden="true"></i>',
										[
											'controller'=>'users',
											'action'=>'deleteannouncements',
											h($ts->id)
										],
										['confirm' => 'Are you sure you want to delete this?','escape' => false, 'class' => 'btn btn-danger btn-xs ' ]
										);
									?>
								</td>
							</tr>
							<?php endforeach; ?> 
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
</div>
<script type="text/javascript">
	$('#events').dataTable( {
		'language': {
			'searchPlaceholder': 'Title, Description, Venue ....'
		}
	});
</script>