<?php
	$session = $this->request->session();
	$user_data = $session->read('Auth.User');
	if(empty($user_data)) {
?>

<style type="text/css">
#aligner {
  margin: auto;
  /*width: 920px;*/
}
.icons {
  position: relative;
  float: left;
  overflow: hidden;
  min-width: 100px;
  max-width: 220px;
  height: 300px;
  width: 100%;
  color: #ffffff;
  text-align: left;
  font-size: 16px;
  display:inline-blocks;
}


.icons img {
  vertical-align: top;
  backface-visibility: hidden;
  height: 200px;
}

.icons figcaption {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 1;
  padding: 10px;
}

.icons h2,
.icons h3,
.icons p {
  margin: 0;
}

.icons h2,
.icons h3 {
  line-height: 1.2em;
}

.icons h2 {
  font-size: 1.1em;
  color: #669900;
}

.icons h3 {
  color: #000000;
  font-size: 1.0em;
  font-weight: normal;
  letter-spacing: 1px;
}

.icons p {
  border-top: 1px solid rgba(0, 0, 0, 0.2);
  font-size: 0.8em;
  margin-top: 0px;
  padding: 12px 0 15px;
  line-height: 1.5em;
  color: #000000;
}

.icons a {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 1;
}

</style>
<div class="container" class="center-block">
	<center>
		<div class="jumbotron">
			<h1>BPH HRMS</h1>
		</div>
	</center> 
	<div class="center-block" id="aligner" style="margin-left: 8%;"> 
		<p>
			<a href="<?= BASE_URL.'timetracker' ?>">
				<figure class="icons">
					<figcaption>
						<center><img src="<?= $this->request->webroot; ?>img/time.png"  height="100%" alt="sample96">
						<br><br>
						<h2>Time tracker</h2></center>
						<p>Track employee time check</p>
					</figcaption>
				</figure>
			</a>
		</p> 
		<p>
			<a href="<?= BASE_URL.'login' ?>">
				<figure class="icons">
					<figcaption>
						<center><img src="<?= $this->request->webroot; ?>img/users.png"  height="100%" alt="sample96"><br><br>
						<h2>Employee</h2></center>
						<p>A person employed for wages</p>
					</figcaption>
				</figure>
			</a>
		</p> 
		<p>
			<?php foreach ($announcements as $key => $value) { ?>
			<div class="col-lg-3 col-sm-4">
				<div class="card-box project-box">
					<h4 class="project-title"><a href="project-view.html"><?=ucfirst($value->name)?></a></h4>
					<small class="block text-ellipsis m-b-15">
						<span class="text-xs">Venue: </span> <span class="text-muted"><?=ucfirst($value->venue)?></span>
					</small>
					<p class="text-muted">
						<?=ucfirst($value->description)?>
					</p>
					<div class="pro-deadline m-b-15">
						<div class="sub-title">
							Date:
						</div>
						<div class="text-muted">
							<?=$value->date?>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</p> 
	</div>
</div>
	<?php } ?>
