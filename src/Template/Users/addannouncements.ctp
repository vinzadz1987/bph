<style type="text/css">
	.error-message {
		font-weight: bold;
		color: red;
	}
</style>
<div id="add_employee" class="modal custom-modal fade" role="dialog">
	<div class="modal-dialog">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<div class="modal-content modal-lg">
			<div class="modal-body">
					<div class="row">
					<?= $this->Form->create($announcements, [ 'class' => 'form' ]) ?>
					<h2>Edit Announcements</h2>
					<div class="col-sm-6">  
					<div class="form-group">
						<div class="form-row">
							<!-- <div class="col-md-8"> -->
								<?= $this->Form->control('name', [ 'class' => 'form-control', 'type' => 'text', 'required' => true, 'label' => 'Title' ]) ?>
							<!-- </div> -->
						</div>
					</div>
					</div>
					<div class="col-sm-6">  
					<div class="form-group">
						<div class="form-row">
							<!-- <div class="col-md-8"> -->
								<?= $this->Form->control('description', [ 'class' => 'form-control', 'type' => 'textarea', 'required' => true ]) ?>
							<!-- </div> -->
						</div>
					</div>
				</div>
					<div class="col-sm-6">  
					<div class="form-group">
						<div class="form-row">
							<!-- <div class="col-md-8"> -->
								<?= $this->Form->control('venue', [ 'class' => 'form-control', 'required' => true ]) ?>
							</div>
						<!-- </div> -->
					</div>
				</div>
				<div class="col-sm-6">  
					<div class="form-group">
						<div class="form-row">
							<?= $this->Form->control('date', [ 'class' => 'form-control datetimepicker', 'type' => 'text', 'required' => true ]) ?>
						</div>
					</div>
				</div>
					<div class="m-t-20 text-center">
						<?= $this->Form->button(__('Save Announcements'),['class'=>'btn btn-primary'] ); ?>
					</div>
					<?= $this->Form->end() ?>
					</div>
				</div>
		</div>
</div>
<a href="#" class="baction hidden" data-toggle="modal" data-target="#add_employee" data-action="add"></a>
<script type="text/javascript">
	$(function () {
		$('.close').click( function() {
			location.href = '<?=BASE_URL?>announcements';
		});
		$(".baction").click();
		$("#phone").mask("999999999999")
		$('.department').select2({
			placeholder:'Enter Designation',
			allowClear: true,
			width: 430
		});
		if($('.errorAlert').hasClass('alert-danger') == true) {
			$("#add_employee").modal({
				show: 'true'
			});
		}
	});
</script>































<!-- <div class="page-wrapper">
<div class="modal-body">
	<div class="card card-register mx-auto mt-5">
		<div class="card-body">
			<?= $this->Form->create($events, [ 'class' => 'form' ]) ?>
			<div class="x_panel">
				<div class="x_title">
					<h2>Add Events</h2>
					<?php 
						echo $this->Html->link(
						'<i class="fa fa-arrow-left" aria-hidden="true"></i> Back To List',
						[
							'controller'=>'users',
							'action'=>'events'
						],
						['escape' => false, 'class' => 'btn btn-success pull-right' ]
						);
					?>
					<ul class="nav navbar-right panel_toolbox"></ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<?= $this->Form->control('name', [ 'class' => 'form-control', 'type' => 'text', 'required' => true ]) ?>
							</div>
						</div>
					</div>
					<!-- <div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<?= $this->Form->input('type', [
									'type' => 'select',
									'class' => 'form-control',
									'required' => true,
									'options' => [ 
										'' => 'Select Type',
										1 => 'Seminar',
										2 => 'Traning'
									]
								]);
								?>
							</div>
						</div>
					</div> -->
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<?= $this->Form->control('description', [ 'class' => 'form-control', 'type' => 'textarea', 'required' => true ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<?= $this->Form->control('venue', [ 'class' => 'form-control', 'required' => true ]) ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<?= $this->Form->control('date', [ 'class' => 'form-control', 'type' => 'text', 'required' => true ]) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
	<!-- 	</div>
	</div>
</div>
</div>
<div class="modal-footer">
	<?php 
      echo $this->Html->link(
        '<i class="fa fa-close" aria-hidden="true"></i> Close',
        [
          'controller'=>'users',
          'action'=>'events'
        ],
        ['escape' => false, 'class' => 'btn btn-danger' ]
      );
    ?>
	<?= $this->Form->button(__('Save'),['class'=>'btn btn-primary','id' => 'saveUser']); ?>
	<?= $this->Form->end() ?>
</div>
<script type="text/javascript">
	$(function () {
		$("#date").datepicker({
			dateFormat: 'yy-mm-dd',
			autoclose: true,
			todayHighlight: true
		});
	});
</script> -->
