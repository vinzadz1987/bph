<div class="main-wrapper">
	<div class="page-wrapper">
	<div class="content container-fluid">
		<div class="row">
			<div class="col-sm-8">
				<h4 class="page-title">Department</h4>
			</div>
			<div class="col-sm-4 text-right m-b-30">
				<a href="#" class="btn btn-primary rounded" data-toggle="modal" data-target="#add_department"><i class="fa fa-plus"></i> Add New Department</a>
			</div>
		</div>
		<?= $this->Flash->render() ?>
		<div class="row">
			<div class="col-md-12">
				<div>
					<table class="table table-striped custom-table m-b-0 datatable">
						<thead>
							<tr>
								<th>#</th>
								<th>Department Name</th>
								<th class="text-right">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($departments as $key => $dept) { ?>
								<tr>
									<td><?= $key ?></td>
									<td><?= $dept->name ?></td>
									<td class="text-right">
										<div class="dropdown">
											<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
											<ul class="dropdown-menu pull-right">
												<li><a href="editdepartments/<?= $dept->id ?>"  data-value='<?= $dept->name ?>' class="edit_department" data-toggle="modal" data-target="#edit_department" title="Edit"><i class="fa fa-pencil m-r-5"></i> Edit</a></li>
												<li>
													<?php echo $this->Html->link(
														'<i class="fa fa-trash-o m-r-5"></i> Delete',
														['controller' => 'Users', 'action' => 'deletedepartments', $dept->id],
														['confirm' => 'Are you sure you wish to delete this department?', 'escape' => false]
													); ?>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="notification-box">
		<?= $this->element('include/notification'); ?>
	</div>
	</div>
	<div id="add_department" class="modal custom-modal fade" role="dialog">
		<?= $this->element('modal/add_department') ?>
	</div>
	<div id="edit_department" class="modal custom-modal fade" role="dialog">
		<?= $this->element('modal/edit_department') ?>
	</div>
</div>

<script type="text/javascript">

	$(function() {

		$('.edit_department').click( function() {
			var href = $(this).attr('href');
			$(".edit_department_frm").attr('action',href);
			$('.edit_department_frm').find("#name").val($(this).attr('data-value'));
		});

	});

</script>