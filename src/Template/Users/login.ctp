<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>


<!DOCTYPE html>
<html>
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
		<title>Login - HRMS </title>

		<?= $this->Html->css([
			'bootstrap.min',
			'font-awesome.min',
			'bph_style.css'
		]);?>

		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
		<?= $this->Html->charset() ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
		<?= $cakeDescription ?>:
		<?= $this->fetch('title') ?>
		</title>
		<?= $this->Html->meta('icon') ?>
		<?= $this->fetch('meta') ?>
		<?= $this->fetch('css') ?>
		<?= $this->fetch('script') ?>
</head>
<style type="text/css">
	

div.message {
    text-align: center;
    cursor: pointer;
    display: block;
    font-weight: normal;
    padding: 8px 1.5rem 0 1.5rem;
    transition: height 300ms ease-out 0s;
    background-color: #a0d3e8;
    color: #626262;
    top: 15px;
    right: 15px;
    z-index: 999;
    overflow: hidden;
    height: 50px;
    line-height: 2.5em;
    box-radius: 5px;
}

div.message:before {
    line-height: 0px;
    font-size: 20px;
    height: 12px;
    width: 12px;
    border-radius: 15px;
    text-align: center;
    vertical-align: middle;
    display: inline-block;
    position: relative;
    left: -11px;
    background-color: #FFF;
    padding: 12px 14px 12px 10px;
    content: "i";
    color: #a0d3e8;
}

div.message.error {
    background-color: #C3232D;
    color: #FFF;
}

div.message.error:before {
    padding: 11px 16px 14px 7px;
    color: #C3232D;
    content: "x";
}
div.message.hidden {
    height: 0;
}

</style>
<body>
    <?php
        $session = $this->request->session();
        $user_data = $session->read('Auth.User');
        if(!empty($user_data)) {
    ?>
    <div class="header">
        <div class="header-left">
            <a href="index.html" class="logo">
                <img src="images/logo.png" width="40" height="40" alt="">
            </a>
            <a href="<?= BASE_URL ?>" class="logo-dark">
                <img src="<?= $this->request->webroot; ?>img/bphlogo.png" width="40" height="40" alt="">
            </a>
        </div>
        <div class="page-title-box pull-left">
            <h3>BILIRAN PROVINCIAL HOSPITAL</h3>
        </div>
        <a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <?php
			$session = $this->request->session();
			$user_data = $session->read('Auth.User');	
			if(!empty($user_data)) {	
		?>
        <ul class="nav navbar-nav navbar-right user-menu pull-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="hasnotifications dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge bg-purple pull-right">3</span></a>
                <div class="dropdown-menu notifications">
                    <div class="topnav-dropdown-header">
                        <span>Notifications</span>
                    </div>
                    <div class="drop-scroll">
                        <ul class="media-list scroll-content">
                            <li class="media notification-message">
                                <a href="activities.html">
                                    <div class="media-left">
                                        <span class="avatar">
                                            <!-- <img alt="John Doe" src="images/user.jpg" class="img-responsive img-circle"> -->
                                        </span>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0 noti-details"><span class="noti-title">John Doe</span> added new task <span class="noti-title">Patient appointment booking</span></p>
                                        <p class="m-0"><span class="notification-time">4 mins ago</span></p>
                                    </div>
                                </a>
                            </li>
                            <li class="media notification-message">
                                <a href="activities.html">
                                    <div class="media-left">
                                        <span class="avatar">V</span>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0 noti-details"><span class="noti-title">Tarah Shropshire</span> changed the task name <span class="noti-title">Appointment booking with payment gateway</span></p>
                                        <p class="m-0"><span class="notification-time">6 mins ago</span></p>
                                    </div>
                                </a>
                            </li>
                            <li class="media notification-message">
                                <a href="activities.html">
                                    <div class="media-left">
                                        <span class="avatar">L</span>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0 noti-details"><span class="noti-title">Misty Tison</span> added <span class="noti-title">Domenic Houston</span> and <span class="noti-title">Claire Mapes</span> to project <span class="noti-title">Doctor available module</span></p>
                                        <p class="m-0"><span class="notification-time">8 mins ago</span></p>
                                    </div>
                                </a>
                            </li>
                            <li class="media notification-message">
                                <a href="activities.html">
                                    <div class="media-left">
                                        <span class="avatar">G</span>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0 noti-details"><span class="noti-title">Rolland Webber</span> completed task <span class="noti-title">Patient and Doctor video conferencing</span></p>
                                        <p class="m-0"><span class="notification-time">12 mins ago</span></p>
                                    </div>
                                </a>
                            </li>
                            <li class="media notification-message">
                                <a href="activities.html">
                                    <div class="media-left">
                                        <span class="avatar">V</span>
                                    </div>
                                    <div class="media-body">
                                        <p class="m-0 noti-details"><span class="noti-title">Bernardo Galaviz</span> added new task <span class="noti-title">Private chat module</span></p>
                                        <p class="m-0"><span class="notification-time">2 days ago</span></p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="topnav-dropdown-footer">
                        <a href="activities.html">View all Notifications</a>
                    </div>
                </div>
            </li>
            <li class="dropdown toolbar-icon-bg hidden-xs">
                <a href="javascript:;" id="open_msg_box" class="hasnotifications"><i class="fa fa-comment-o"></i> <span class="badge bg-purple pull-right">8</span></a>
            </li>   
            <li class="dropdown">
                <a href="profile.html" class="dropdown-toggle user-link" data-toggle="dropdown" title="Admin">
                    <!-- <span class="user-img"><img class="img-circle" src="images/user.jpg" width="40" alt="Admin"> -->
                    <span class="status online"></span></span>
                    <span>Admin</span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="profile.html">My Profile</a></li>
                    <li><a href="edit-profile.html">Edit Profile</a></li>
                    <li><a href="settings.html">Settings</a></li>
                    <li><a href="<?= BASE_URL.'users/logout'?>">Logout</a></li>
                </ul>
            </li>
        </ul>
        <?php } ?>
        <div class="dropdown mobile-user-menu pull-right">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
            <ul class="dropdown-menu pull-right">
                <li><a href="profile.html">My Profile</a></li>
                <li><a href="edit-profile.html">Edit Profile</a></li>
                <li><a href="settings.html">Settings</a></li>
                <li><a href="login.html">Logout</a></li>
            </ul>
        </div>
    </div>
    <?php } ?>
    <div class="container clearfix">
			<div class="account-page">
				<div class="container">
					<h3 class="account-title">HRMS Login</h3>
					<div class="account-box">
						<div class="account-wrapper">
							<div class="account-logo">
								<a href="index.html"><img src="<?= $this->request->webroot; ?>img/bphlogo.png" alt="Focus Technologies"></a>
							</div>
								<?= $this->Form->create() ?>
		 							<?= $this->Flash->render() ?>
								<div class="form-group form-focus">
                                    <div class="col-xs-12">
    									<label class="control-label" style="padding-left: 10px">Username</label>
    									<?= $this->Form->control('email', [ 'class' => 'form-control floating', 'label' => false ] ) ?>
                                    </div>
								</div>
    							<div class="form-group form-focus">
                                    <div class="col-xs-12">
                                        <label class="control-label" style="padding-left: 10px">Password</label>
    									<?= $this->Form->control('password', [ 'class' => 'form-control floating', 'label' => false] ) ?>
                                    </div>
								</div>
								<div class="form-group text-center">
									<?= $this->Form->button('Login', [ 'class' => 'btn btn-primary btn-block account-btn' ]) ?>
								</div>
								<?= $this->Form->end() ?>
								<!-- <div class="text-center">
									<a href="forgot-password.html">Forgot your password?</a>
								</div> -->
						</div>
					</div>
				</div>
		</div>
    </div>
    <footer>
    </footer>
	<div class="sidebar-overlay" data-reff="#sidebar"></div>
	<script type="text/javascript" src="https://dreamguys.co.in/hrms/js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="https://dreamguys.co.in/hrms/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://dreamguys.co.in/hrms/js/app.js"></script>
	</body>
</html>








