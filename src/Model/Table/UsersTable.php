<?php
	namespace App\Model\Table;

	use Cake\ORM\Table;
	use Cake\Validation\Validator;
	use Cake\ORM\RulesChecker;

	class UsersTable extends Table
	{
		public function initialize(array $config) {
			$this->belongsTo('Designations')
				->setForeignkey('designations')
				->setJoinType('INNER');
		}

		public function validationDefault(Validator $validator)
		{
			$validator
				->notEmpty('firstname', 'A firstname is required')
				->notEmpty('lastname', 'A lastname is required')
				->notEmpty('middlename', 'A middlename is required')
				->notEmpty('username', 'A username is required')
				->notEmpty('email', 'A email is required')
				->notEmpty('employeeid', 'A employeeid is required')
				->notEmpty('datehired', 'A datehired is required')
				->notEmpty('phone', 'A phone is required')
				->notEmpty('department', 'A department is required')
				->notEmpty('designation', 'A designation is required')
				->notEmpty('pincode', 'A pincode is required');
			$validator
			->add('password',[
				'match'=>[
					'rule'=> ['compareWith','password2'],
					'message'=>'The passwords does not match!',
				]
			])
			->notEmpty('password');
			$validator
			->add('password2',[
				'match'=>[
					'rule'=> ['compareWith','password'],
					'message'=>'The passwords does not match!',
				]
			])
			->notEmpty('password2');

			return $validator;
		}

		public function buildRules(RulesChecker $rules)
		{
			$rules->add($rules->isUnique(['email']));
			$rules->add($rules->isUnique(['userid']));
			return $rules;
		}

	}
