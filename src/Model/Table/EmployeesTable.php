<?php
	namespace App\Model\Table;

	use Cake\ORM\Table;
	use Cake\Validation\Validator;
	use Cake\ORM\RulesChecker;

	class EmployeesTable extends Table
	{
		public function initialize(array $config) {
			$this->belongsTo('Designations')
				->setForeignkey('designations')
				->setJoinType('INNER');
		}

		public function validationDefault(Validator $validator)
		{
			$validator

				->notEmpty('firstname', 'A firstname is required')
				->notEmpty('lastname', 'A lastname is required')
				->notEmpty('middlename', 'A middlename is required')
				->notEmpty('username', 'A username is required')
				->notEmpty('email', 'A email is required')
				->notEmpty('employeeid', 'A employeeid is required')
				->notEmpty('datehired', 'A datehired is required')
				->notEmpty('phone', 'A contact no is required')
				->notEmpty('department', 'A department is required')
				->notEmpty('designation', 'A designation is required')
				->notEmpty('birthdate', 'A birthdate is required')
				->notEmpty('sex', 'A sex is required')
				->notEmpty('civilstatus', 'A civilstatus is required')
				->notEmpty('place_of_birth', 'A birthdate is required')
				->notEmpty('birthdate', 'A birthdate is required')
				->notEmpty('trainings', 'A trainings is required')
				->notEmpty('promotions', 'A promotions is required')
				->notEmpty('role', 'A role is required')
				->notEmpty('leave_reason', 'A leave_reason is required')
				->notEmpty('daily_rate', 'A daily_rate is required')
				->notEmpty('basic_salary', 'A basic_salary is required')
				->notEmpty('pincode', 'A pincode is required');

			return $validator;
		}

		public function buildRules(RulesChecker $rules)
		{
			$rules->add($rules->isUnique(['email']));
			$rules->add($rules->isUnique(['pincode']));
			$rules->add($rules->isUnique(['employeeid']));
			return $rules;
		}

	}
