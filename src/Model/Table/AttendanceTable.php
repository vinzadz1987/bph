<?php
	namespace App\Model\Table;

	use Cake\ORM\Table;
	use Cake\Validation\Validator;
	use Cake\ORM\RulesChecker;
	use Cake\Utility\Text;

	class AttendanceTable extends Table
	{
		public function initialize(array $config) {
			$this->belongsTo('Designations')
				->setForeignkey('designations')
				->setJoinType('INNER');
		}

		public function validationDefault(Validator $validator)
		{
			$validator

				->notEmpty('firstname', 'A firstname is required')
				->notEmpty('lastname', 'A lastname is required')
				->notEmpty('middlename', 'A middlename is required')

				->notEmpty('username', 'A username is required')
				->add('username', [
			        'length' => [
			            'rule' => ['minLength', 10],
			            'message' => 'username need to be at least 10 characters long',
			        ]
			    ])
				->notEmpty('email', 'A email is required')

				->notEmpty('employeeid', 'A employeeid is required')
				->notEmpty('datehired', 'A datehired is required')

				->notEmpty('phone', 'A contact no is required')

				->notEmpty('department', 'A department is required')
				->notEmpty('designation', 'A designation is required');

			// $validator
			// ->add('password',[
			// 	'match'=>[
			// 		'rule'=> ['compareWith','confirm_password'],
			// 		'message'=>'The passwords does not match!',
			// 	]
			// ])
			// ->notEmpty('password');
			// $validator
			// ->add('confirm_password',[
			// 	'match'=>[
			// 		'rule'=> ['compareWith','password'],
			// 		'message'=>'The passwords does not match!',
			// 	]
			// ])
			// ->notEmpty('confirm_password');

			return $validator;
		}

		public function buildRules(RulesChecker $rules)
		{
			$rules->add($rules->isUnique(['email']));
			$rules->add($rules->isUnique(['employeeid']));
			return $rules;
		}

	}
