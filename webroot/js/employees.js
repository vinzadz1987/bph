var $sidebarOverlay = $(".task-overlay");
$(".task-chat").on("click", function(e) {
	var $target = $($(this).attr("href"));
	if ($target.length) {
		$target.toggleClass("opened");
		$sidebarOverlay.toggleClass("opened");
		$("body").toggleClass("menu-opened");
		$sidebarOverlay.attr("data-reff", $(this).attr("href"))
	}
	e.preventDefault()
});
$sidebarOverlay.on("click", function(e) {
	var $target = $($(this).attr("data-reff"));
	if ($target.length) {
		$target.removeClass("opened");
		$("body").removeClass("menu-opened");
		$(this).removeClass("opened")
	}
	e.preventDefault()
});


var $sidebarOverlay = $(".sidebar-overlay");
$("#mobile_btn").on("click", function(e) {
	var $target = $($(this).attr("href"));
	if ($target.length) {
		$target.toggleClass("opened");
		$sidebarOverlay.toggleClass("opened");
		$("body").toggleClass("menu-opened");
		$sidebarOverlay.attr("data-reff", $(this).attr("href"))
	}
	e.preventDefault()
});
$sidebarOverlay.on("click", function(e) {
	var $target = $($(this).attr("data-reff"));
	if ($target.length) {
		$target.removeClass("opened");
		$("body").removeClass("menu-opened");
		$(this).removeClass("opened")
		$(".main-wrapper").removeClass("slide-nav-toggle");
	}
	e.preventDefault()
});